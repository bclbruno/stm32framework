/*
 * fw_stdout.h
 *
 *  Created on: 15 de abr de 2018
 *      Author: Bruno
 */

#ifndef FW_STDOUT_H_
#define FW_STDOUT_H_

void fw_putch(unsigned char ch);

#endif /* FW_STDOUT_H_ */

/*
 * RF24Network.h
 *
 *  Created on: 26 de out de 2017
 *      Author: Bruno
 */

#ifndef INCLUDE_RF24_RF24NETWORK_H_
#define INCLUDE_RF24_RF24NETWORK_H_

#include <RF24/RF24Device.h>
#include "RF24Network_def.h"

#ifdef RF24_NET_SERIAL_DEBUG
	#include "Serial.h"
	extern serial::CSerial* rf24_serial;

	inline void set_rf24_net_serial_debug(serial::CSerial* serial)
	{
		rf24_serial = serial;
	}
#endif

namespace rf24
{
#pragma pack(1)
	struct RF24Frame
	{
		NodeAddress from_node;
		NodeAddress to_node;
		MessageID	id;
		MessageType type;
		MessageData data;
	};
#pragma pack()

	const uint8_t NETWORK_MAX_CHILDREN = 6;


	class RF24Network
	{
	public:
		RF24Network(RF24Device& radio);

		bool init(const NodeAddress node);

		MessageType update(RF24Frame* message);

		uint8_t send_message(RF24Frame* message) const;

		inline NodeAddress get_address() const { return node_address_; }

		static bool is_valid_address(NodeAddress node);

		void set_node_address(const NodeAddress node);

		void power_up();
		void power_down();

	private:
		bool is_ack_type(MessageType type) const;

		uint8_t get_message_destiny(const NodeAddress node) const;

		PipeAddress children_pipe(const uint8_t child) const;

		MessageType process_message(RF24Frame* message) const;
		void forward_message(RF24Frame* message) const;
		void send_nack(RF24Frame* message) const;

		RF24Device& radio_device_;

		NodeAddress node_address_;

		//Stores children pipe address
		//Child 0 is the node itself so it can receive messages from the parent
		PipeAddress children_pipe_[NETWORK_MAX_CHILDREN];

		uint8_t payload_[sizeof(RF24Frame)];
	};
}



#endif /* INCLUDE_RF24_RF24NETWORK_H_ */

/*
 * RF24Network_def.h
 *
 *  Created on: 28 de out de 2017
 *      Author: Bruno
 */

#ifndef INCLUDE_RF24_RF24NETWORK_DEF_H_
#define INCLUDE_RF24_RF24NETWORK_DEF_H_

#include "NRF24_def.h"

//#define RF24_NET_SERIAL_DEBUG

namespace rf24 {

	typedef uint16_t NodeAddress;
	typedef uint8_t	 MessageID;
	typedef uint8_t  MessageType;
	typedef uint32_t MessageData;

	const NodeAddress	MASTER_ADDRESS  = 0x0000;
	const PipeAddress	MASTER_PIPE		= 0x7FFFFFFFFF;

	enum NETWORK_COMMANDS : MessageType
	{
		NETWORK_IDLE			= 0x00,

		//0x01 - 0xCF = APP defined commands

		NETWORK_IS_ACK_BOTTOM	= 0xC0,

		NETWORK_ADDR_RESPONSE	= 0xD0,
		NETWORK_PING			= 0xD1,
		NETWORK_PONG			= 0xD2,

		NETWORK_FIRST_FRAGMENT	= 0xE4,
		NETWORK_MID_FRAGMENT	= 0xE5,
		NETWORK_LAST_FRAGMENT	= 0xE6,

		NETWORK_IS_ACK_TOP		= 0xF0,
		NETWORK_ACK				= 0xF1,
		NETWORK_NACK			= 0xF2,
	};

}  // namespace rf24

#endif /* INCLUDE_RF24_RF24NETWORK_DEF_H_ */

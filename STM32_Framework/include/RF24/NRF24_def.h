/*
 * NRF24_def.h
 *
 *  Created on: 28 de out de 2017
 *      Author: Bruno
 */

#ifndef INCLUDE_RF24_NRF24_DEF_H_
#define INCLUDE_RF24_NRF24_DEF_H_

//#define RF24_SERIAL_DEBUG

namespace rf24{
	typedef uint64_t PipeAddress;

	enum SETUP_RETR_BITS : uint8_t
	{
		SETUP_RETR_ARD_250us	= 0x00,
		SETUP_RETR_ARD_500us	= 0x01,
		SETUP_RETR_ARD_750us	= 0x02,
		SETUP_RETR_ARD_1000us	= 0x03,
		SETUP_RETR_ARD_1250us	= 0x04,
		SETUP_RETR_ARD_1500us	= 0x05,
		SETUP_RETR_ARD_1750us	= 0x06,
		SETUP_RETR_ARD_2000us	= 0x07,
		SETUP_RETR_ARD_2250us	= 0x08,
		SETUP_RETR_ARD_2500us	= 0x09,
		SETUP_RETR_ARD_2750us	= 0x0A,
		SETUP_RETR_ARD_3000us	= 0x0B,
		SETUP_RETR_ARD_3250us	= 0x0C,
		SETUP_RETR_ARD_3500us	= 0x0D,
		SETUP_RETR_ARD_3750us	= 0x0E,
		SETUP_RETR_ARD_4000us	= 0x0F,

		SETUP_RETR_ARC_DISABLE	= 0x00,
		SETUP_RETR_ARC_1RETRY	= 0x10,
		SETUP_RETR_ARC_2RETRY	= 0x20,
		SETUP_RETR_ARC_3RETRY	= 0x30,
		SETUP_RETR_ARC_4RETRY	= 0x40,
		SETUP_RETR_ARC_5RETRY	= 0x50,
		SETUP_RETR_ARC_6RETRY	= 0x60,
		SETUP_RETR_ARC_7RETRY	= 0x70,
		SETUP_RETR_ARC_8RETRY	= 0x80,
		SETUP_RETR_ARC_9RETRY	= 0x90,
		SETUP_RETR_ARC_10RETRY	= 0xA0,
		SETUP_RETR_ARC_11RETRY	= 0xB0,
		SETUP_RETR_ARC_12RETRY	= 0xC0,
		SETUP_RETR_ARC_13RETRY	= 0xD0,
		SETUP_RETR_ARC_14RETRY	= 0xE0,
		SETUP_RETR_ARC_15RETRY	= 0xF0,

		SETUP_RETR_ARD_MASK	  	= 0x0F,
		SETUP_RETR_ARD_NOT_MASK = 0xF0,

		SETUP_RETR_ARC_MASK	  	= 0xF0,
		SETUP_RETR_ARC_NOT_MASK = 0x0F,
	};

	enum CRC_BITS : uint8_t
	{
		RF24_CRC_DISABLED 	= 0x00,
		RF24_CRC_8			= 0x08,
		RF24_CRC_16			= 0x0C,

		RF24_CRC_MASK		= 0x0C,
		RF24_CRC_NOT_MASK	= 0xF3,
	};

	enum RF24_CMDS : uint8_t
	{
		R_REGISTER		= 0x00,
		W_REGISTER		= 0x20,
		R_RX_PAYLOAD	= 0x61,
		W_TX_PAYLOAD	= 0xA0,
		FLUSH_TX		= 0xE1,
		FLUSH_RX		= 0xE2,
		REUSE_TX_PL		= 0xE3,
		R_RX_PL_WID		= 0x60,
		W_ACK_PAYLOAD	= 0xA8,
		W_TX_PL_NO_ACK	= 0xB0,
		NOP				= 0xFF,
	};

	enum RF24_REGISTERS : uint8_t
	{
		NRF_CONFIG	= 0x00,
		EN_AA		= 0x01,
		EN_RXADDR	= 0x02,
		SETUP_AW	= 0x03,
		SETUP_RETR	= 0x04,
		RF_CH		= 0x05,
		RF_SETUP	= 0x06,
		NRF_STATUS	= 0x07,
		OBSERVE_TX	= 0x08,
		CD			= 0x09,
		RX_ADDR_P0	= 0x0A,
		RX_ADDR_P1	= 0x0B,
		RX_ADDR_P2	= 0x0C,
		RX_ADDR_P3	= 0x0D,
		RX_ADDR_P4	= 0x0E,
		RX_ADDR_P5	= 0x0F,
		TX_ADDR		= 0x10,
		RX_PW_P0	= 0x11,
		RX_PW_P1	= 0x12,
		RX_PW_P2	= 0x13,
		RX_PW_P3	= 0x14,
		RX_PW_P4	= 0x15,
		RX_PW_P5	= 0x16,
		FIFO_STATUS	= 0x17,
		DYNPD		= 0x1C,
		FEATURE		= 0x1D,

		REGISTER_MASK = 0x1F,
	};

	enum NRF_CONFIG_BITS : uint8_t
	{
		PRIM_RX		= 0x01,
		PRIM_TX		= 0xFE,	//NOT PRIM_RX
		PWR_UP		= 0x02,
		PWR_DOWN	= 0xFD, //NOT PWR_UP
		CRCO		= 0x04,
		EN_CRC		= 0x08,
		MASK_MAX_RT = 0x10,
		MASK_TX_DS	= 0x20,
		MASK_RX_DR	= 0x40,
	};

	enum ENAA_BITS : uint8_t
	{
		ENAA_P0	= 0x01,
		ENAA_P1	= 0x02,
		ENAA_P2	= 0x04,
		ENAA_P3	= 0x08,
		ENAA_P4	= 0x10,
		ENAA_P5	= 0x20,
	};

	enum EN_RXADDR_BITS : uint8_t
	{
		ERX_P0	= 0x01,
		ERX_P1	= 0x02,
		ERX_P2	= 0x04,
		ERX_P3	= 0x08,
		ERX_P4	= 0x10,
		ERX_P5	= 0x20,
	};

	enum SETUP_AW_BITS : uint8_t
	{
		SETUP_AW_3_BYTES	= 0x01,
		SETUP_AW_4_BYTES	= 0x02,
		SETUP_AW_5_BYTES	= 0x03,
	};

	enum RF_SETUP_BITS : uint8_t
	{
		RF_SETUP_RF_PWR		= 0x06,
		RF_SETUP_DR_HIGH	= 0x08,
		RF_SETUP_PLL_LOCK	= 0x10,
		RF_SETUP_RF_DR_LOW	= 0x20,
		RF_SETUP_CONT_WAVE	= 0x80,
	};

	enum DATA_RATE : uint8_t
	{
		RF_SETUP_DR_1MBPS	= 0x00,
		RF_SETUP_DR_2MBPS	= 0x08,
		RF_SETUP_DR_250KBPS	= 0x20,

		RF_SETUP_DR_MASK	= 0x28,
		RF_SETUP_DR_NOT_MASK= 0xD7,
	};

	enum PA_LEVEL : uint8_t
	{
		RF_SETUP_PA_MIN		= 0x00,
		RF_SETUP_PA_LOW		= 0x02,
		RF_SETUP_PA_HIGH	= 0x04,
		RF_SETUP_PA_MAX		= 0x06,

		RF_SETUP_PA_MASK	= 0x06,
		RF_SETUP_PA_NOT_MASK= 0xF9,
	};

	enum NRF_STATUS_BITS : uint8_t
	{
		TX_FULL		= 0x01,	//TX FIFO full flag
		RX_P_NO_MASK= 0x0e,	//Data pipe nb for reading (mask)
		MAX_RT		= 0x10,	//Max numb of TX retransmits int
		TX_DS		= 0x20,	//Data sent TX FIFO int
		RX_DR		= 0x40,	//Data ready RX FIFO int
	};

	enum FIFO_STATUS_BITS : uint8_t
	{
		FIFO_RX_EMPTY	= 0x01,
		FIFO_RX_FULL	= 0x02,
		FIFO_TX_EMPTY	= 0x10,
		FIFO_TX_FULL	= 0x20,
		FIFO_TX_REUSE	= 0x40,
	};

	enum NETWORK_MSG_STATUS : uint8_t
	{
		MSG_TIMEOUT		= 0x00,
		MSG_SENT		= 0x01,
		INVALID_ADDRESS	= 0x02,
		SPI_TIMEOUT		= 0x03,
	};

#ifdef RF24_SERIAL_DEBUG
	#define IF_RF24_SERIAL_DEBUG(x) x;
#else
	#define IF_RF24_SERIAL_DEBUG(x)
#endif

}//namespace rf24

#endif /* INCLUDE_RF24_NRF24_DEF_H_ */

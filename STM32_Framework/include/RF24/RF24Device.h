/*
 * CRF24.h
 *
 *  Created on: 18 de set de 2017
 *      Author: Bruno
 */

#ifndef INCLUDE_CRF24_H_
#define INCLUDE_CRF24_H_

#include <stdint.h>
#include "GPIO.h"
#include "SPI.h"
#include "NRF24_def.h"

#ifdef RF24_SERIAL_DEBUG
	#include "Serial.h"
	extern serial::CSerial* rf24_serial;

	inline void set_rf24_serial_debug(serial::CSerial* serial)
	{
		rf24_serial = serial;
	}
#endif

namespace rf24{
	class RF24Device
	{
	public:
		RF24Device(const gpio::pin_t csn_pin, const gpio::pin_t ce_pin, spi::SPI& spi);
		uint8_t init();

		bool read(void* buffer, const uint8_t buffer_length) const;
		uint8_t write(const void* buffer, const uint8_t buffer_length) const;

		void start_listening() const;
		void stop_listening()  const;

		bool available() const;

		void set_PA_level(const PA_LEVEL level) const;

		bool set_data_rate(const DATA_RATE data_rate) const;

		void set_retries(const SETUP_RETR_BITS retries_flag) const;

		void	set_payload_size(const uint8_t size);
		uint8_t get_payload_size() const { return payload_size_; }

		void open_writing_pipe(const PipeAddress address) const;
		void open_reading_pipe(const uint8_t pipe, const PipeAddress address);
		void close_reading_pipe(const uint8_t pipe) const;

		void power_up()   const;
		void power_down() const;

	private:
		uint8_t get_status() const;

		uint8_t write_register(const uint8_t reg, const uint8_t value) const;
		uint8_t write_register(const uint8_t reg, const void* const buf, uint8_t len) const;

		uint8_t read_register(const uint8_t reg) const;
		uint8_t read_register(const uint8_t reg, void* buffer, uint8_t buffer_length) const;

		uint8_t write_payload(const void* buffer, const uint8_t buffer_length) const;
		uint8_t read_payload(void* buffer, const uint8_t buffer_length) const;

		inline uint8_t flush_tx() const { return spi_transfer(FLUSH_TX); }
		inline uint8_t flush_rx() const { return spi_transfer(FLUSH_RX); }

		void set_channel(const uint8_t channel) const;
		void set_crc_length(const uint8_t length) const;

		uint8_t spi_transfer(const uint8_t cmd) const;

		gpio::GPIO csn_pin_;
		gpio::GPIO ce_pin_;

		spi::SPI&	spi_;

		uint8_t payload_size_;
		bool	dynamic_payloads_enabled_;

		PipeAddress pipe0_reading_address_;
	};
}//namespace rf24

#endif /* INCLUDE_CRF24_H_ */

/*
 * SYSTICK.h
 *
 *  Created on: 16 de set de 2017
 *      Author: Bruno
 */


#ifndef INCLUDE_SYSTICK_H_
#define INCLUDE_SYSTICK_H_

#include <stdint.h>

namespace core
{
	void config_systick(const uint32_t ticks);

	void delay(const uint32_t ms);
	uint32_t millis();
	uint32_t micros();
}

#endif /* INCLUDE_SYSTICK_H_ */

/*
 * GPIO.h
 *
 *  Created on: 12 de set de 2017
 *      Author: Bruno
 */

#ifndef INCLUDE_GPIO_H_
#define INCLUDE_GPIO_H_

#include <FWK_def.h>

namespace gpio{

	typedef enum
	{
		PIN_0 = 0,
		PIN_1,
		PIN_2,
		PIN_3,
		PIN_4,
		PIN_5,
		PIN_6,
		PIN_7,
		PIN_8,
		PIN_9,
		PIN_10,
		PIN_11,
		PIN_12,
		PIN_13,
		PIN_14,
		PIN_15,
		PIN_ALL,
	}pin_no_t;

	typedef enum : uint32_t
	{
		PORT_A,
		PORT_B,
		PORT_C,
		PORT_D,
		PORT_E,
		PORT_F,
		PORT_G,
	}port_t;

	typedef enum : uint32_t
	{
		INPUT_FLOATING,
		INPUT_PULL_UP,
		INPUT_PULL_DOWN,
		ANALOG,
		OUTPUT_OPEN_DRAIN,
		OUTPUT_PUSH_PULL,
		AF_OPEN_DRAIN,
		AF_PUSH_PULL,
	}pin_config_t;

	typedef enum : uint32_t
	{
		SPEED_2MHZ,
		SPEED_10MHZ,
		SPEED_50MHZ,
		SPEED_INPUT,
	}pin_speed_t;

	typedef struct
	{
		port_t		port;
		pin_no_t	pin_no;
	}pin_t;

	typedef struct
	{
		pin_config_t	config;
		pin_speed_t		speed;
	}pin_def_t;

	class GPIO
	{
	public:
		void set_high() const;
		void set_low() const;

		void write(const bool state) const;
		bool read() const;

		void init(const pin_config_t config, const pin_speed_t speed);
		inline void init(const pin_def_t gpio_def) { init(gpio_def.config, gpio_def.speed); }

		void enable_ext_int() const;
		static void enable_clock(const port_t port);

		GPIO(const pin_t pin) : GPIO(pin.port, pin.pin_no) {};
		GPIO(const port_t port, const pin_no_t pin_no);

	private:
		port_t  get_port_id() const;
		uint8_t get_mode(const pin_config_t config) const;
		uint8_t get_speed(const pin_speed_t speed) const;

		HWRegister	register_;

		const uint32_t	pin_mask_;
		const pin_no_t	pin_no_;
		const port_t	port_;
	};
}

#endif /* INCLUDE_GPIO_H_ */

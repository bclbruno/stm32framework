/*
 * System.h
 *
 *  Created on: 14 de set de 2017
 *      Author: Bruno
 */

#ifndef INCLUDE_SYSTEM_H_
#define INCLUDE_SYSTEM_H_

#include <stdint.h>
#include "SYSTICK.h"

namespace core{
	const uint32_t	SYSCLK_FREQ			= 72000000;
	const uint32_t	VECT_TAB_OFFSET		= 0x00000000;
	const uint32_t	STM32_DELAY_US_MULT	= SYSCLK_FREQ / 6000000L;

	enum
	{
		IHS_CLK,		//Internal High Speed Clock
		EHS_CLK,		//External High Speed Clock
		EHS_CLK_DIV_2,	//External High Speed Clock divided by 2
		ILS_CLK,		//Internal Low Speed Clock
		ELS_CLK,		//External Low Speed Clock
		PLL_CLK,		//PLL Output Clock
	};

	enum
	{
		PRE_SCALE_1 = 0,//Output clock equals to the input clock
		PRE_SCALE_2,	//Output clock = (Input clock)/2
		PRE_SCALE_4,	//Output clock = (Input clock)/4
		PRE_SCALE_8,	//Output clock = (Input clock)/8
		PRE_SCALE_16,	//Output clock = (Input clock)/16
		PRE_SCALE_32,	//Output clock = (Input clock)/32
		PRE_SCALE_64,	//Output clock = (Input clock)/64
		PRE_SCALE_128,	//Output clock = (Input clock)/128
		PRE_SCALE_512,	//Output clock = (Input clock)/256
	};

	enum
	{
		ADC_PRE_SCALE_2 = 0,//Output clock = (Input clock)/2
		ADC_PRE_SCALE_4,	//Output clock = (Input clock)/4
		ADC_PRE_SCALE_6,	//Output clock = (Input clock)/6
		ADC_PRE_SCALE_8,	//Output clock = (Input clock)/8
	};

	void reset_clock();

	void enable_clock(const uint32_t clock);
	void disable_clock(const uint32_t clock);
	bool is_clock_enabled(const uint32_t clock);

	void set_sysclk_source(const uint32_t source);
	uint32_t get_sysclk_source();
	void set_pll_source(const uint32_t clk_source, const uint32_t pll_mult);

	void set_peripheral_clk(const uint32_t clk_pre_scale);
	void set_hs_peripheral_clk(const uint32_t clk_pre_scale);
	void set_ls_peripheral_clk(const uint32_t clk_pre_scale);

	void set_vector_table();

	void set_adc_clock(const uint32_t clk_pre_scale);

	static inline void delay_us(uint32_t us)
	{
		us *= STM32_DELAY_US_MULT;

		/* fudge for function call overhead  */
		us--;
		asm volatile("   mov r0, %[us]          \n\t"
					 "1: subs r0, #1            \n\t"
					 "   bhi 1b                 \n\t"
					 :
					 : [us] "r" (us)
					 : "r0");
	}

}//namespace system

#endif /* INCLUDE_SYSTEM_H_ */

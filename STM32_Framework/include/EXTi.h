/*
 * EXTi.h
 *
 *  Created on: 1 de out de 2017
 *      Author: Bruno
 */

#ifndef INCLUDE_EXTI_H_
#define INCLUDE_EXTI_H_

#include "FWK_def.h"
#include "GPIO.h"

typedef void* voidPtr;
typedef void (*voidFuncPtr)(voidPtr);

namespace exti{
	typedef enum : uint32_t
	{
		EXTI0	= 0,
		EXTI1,
		EXTI2,
		EXTI3,
		EXTI4,
		EXTI5,
		EXTI6,
		EXTI7,
		EXTI8,
		EXTI9,
		EXTI10,
		EXTI11,
		EXTI12,
		EXTI13,
		EXTI14,
		EXTI15,
	}EXTI_line;

	typedef enum
	{
		EXTI_NONE_EDGE,
		EXTI_FALLING_EDGE,
		EXTI_RISING_EDGE,
		EXTI_BOTH_EDGE,
	}EXTI_trigger;

	class CEXTI
	{
	public:
		CEXTI(const gpio::pin_t pin) : CEXTI(pin.port, pin.pin_no) {};
		CEXTI(const gpio::port_t port, const gpio::pin_no_t pin_no);
		void attach_callback(voidFuncPtr callback, voidPtr parameter) const;
		void configure_pin(const gpio::pin_config_t config, const gpio::pin_speed_t mode);
		inline void configure_pin(const gpio::pin_def_t def){ configure_pin(def.config, def.speed); };
		void deattach_callback() const;
		void trigger_selection(const EXTI_trigger trigger) const;

		void enable_interrupt() const;
		void disable_interrupt() const;

		void enable_event() const;
		void disable_event() const;
	private:
		gpio::GPIO int_pin_;
		uint32_t line_;
	};
}



#endif /* INCLUDE_EXTI_H_ */

/*
 * ADC.h
 *
 *  Created on: 16 de dez de 2017
 *      Author: Bruno
 */

#ifndef INCLUDE_ADC_H_
#define INCLUDE_ADC_H_

#include "FWK_def.h"
#include <ADC_defs.h>

namespace adc
{
	class ADC
	{
	public:
		ADC(const device_t device, const channel_t channel);

		void init();
		uint16_t single_read() const;
	private:
		void init_gpio() const;
		void enable_clock() const;

	private:
		HWRegister	register_;
		device_t	device_;
		uint8_t 	channel_;
	};
}



#endif /* INCLUDE_ADC_H_ */

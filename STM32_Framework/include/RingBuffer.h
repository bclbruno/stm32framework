/*
 * RingBuffer.h
 *
 *  Created on: 21 de out de 2017
 *      Author: Bruno
 */

#ifndef INCLUDE_RINGBUFFER_H_
#define INCLUDE_RINGBUFFER_H_

#include <cstdint>

template <typename T, const std::size_t buffer_size>
class RingBuffer
{
public:
	inline bool is_empty() const { return tail_ == head_; }
	inline bool has_data() const { return tail_ != head_; }
	inline bool is_full()  const { return ((tail_ + 1)%buffer_size == head_); }

	inline uint32_t size() const
	{
		if(head_ > tail_)
		{
			return (buffer_size + tail_ - head_);
		}
		else
		{
			return (tail_ - head_);
		}
	}

	inline void insert(const T data)
	{
		if(is_full())
		{
			remove();
		}

		buffer_[tail_] = data;

		tail_ = (tail_ + 1)%buffer_size;
	}

	inline T remove()
	{
		const T data = buffer_[head_];

		head_ = (head_ + 1)%buffer_size;
		return data;
	}

	inline T remove_last()
	{
		const T data = buffer_[tail_];

		if(tail_ == 0)
		{
			tail_ = buffer_size - 1;
		}
		else
		{
			--tail_;
		}

		return data;
	}

	inline T peek_first()
	{
		return buffer_[head_];
	}

	inline void clear()
	{
		head_ = 0;
		tail_ = 0;
	}

private:
	T buffer_[buffer_size];
	volatile uint16_t head_;
	volatile uint16_t tail_;
};

#endif /* INCLUDE_RINGBUFFER_H_ */

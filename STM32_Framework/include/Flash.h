/*
 * Flash.h
 *
 *  Created on: 14 de set de 2017
 *      Author: Bruno
 */

#ifndef INCLUDE_FLASH_H_
#define INCLUDE_FLASH_H_

#include <stdint.h>

namespace core{
	namespace flash{
		const uint32_t ACR_PREFETCH_BUFFER_BIT	= 0x00000010;

		const uint32_t ACR_LATENCY_MASK	= 0x00000003;

		typedef enum : uint32_t
		{
			ZERO_WAIT_STATE	= 0x00000000,
			ONE_WAIT_STATE	= 0x00000001,
			TWO_WAIT_STATE	= 0x00000002,
		}latency_t;

		void enable_prefetch();

		void set_latency(latency_t latency);
	}//namespace flash
}//namespace core

#endif /* INCLUDE_FLASH_H_ */

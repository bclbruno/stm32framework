/*
 * Serial.h
 *
 *  Created on: 6 de out de 2017
 *      Author: Bruno
 */

#ifndef INCLUDE_SERIAL_H_
#define INCLUDE_SERIAL_H_

#include "FWK_def.h"
#include "RingBuffer.h"
#include "Print.h"

namespace serial{
	enum : uint8_t
	{
		WORD_8b = 0x08,
		WORD_9b = 0x09,
	};

	enum : uint8_t
	{
		PARITY_NONE,
		PARITY_EVEN,
		PARITY_ODD,
	};

	enum : uint8_t
	{
		STOP_BIT_1,
		STOP_BIT_0_5,
		STOP_BIT_2,
		STOP_BIT_1_5,
	};

	typedef enum
	{
		SERIAL1,
		SERIAL2,
		SERIAL3,
	}device_t;

	class Serial : public print::Print
	{
	public:
		Serial(const device_t device);

		void init(const uint32_t baud_rate, const uint16_t config) const;
		inline void init(const uint32_t baud_rate) const { init(baud_rate, WORD_8b | PARITY_NONE | STOP_BIT_1); }

		inline bool has_data() const { return rx_buffer_.has_data(); }

		virtual void write(const uint8_t byte) const;
		inline uint8_t read() { return rx_buffer_.remove(); }

		void callback();

	private:
		inline void init_gpio() const;
		void enable_clk() const;
		void enable_interrupt() const;

	private:
		HWRegister	register_;
		device_t	device_;

		RingBuffer<uint8_t, 128> rx_buffer_;
	};
}//namespace serial

#endif /* INCLUDE_SERIAL_H_ */

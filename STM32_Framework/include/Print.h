/*
 * Print.h
 *
 *  Created on: 21 de abr de 2018
 *      Author: Bruno
 */

#ifndef INCLUDE_PRINT_H_
#define INCLUDE_PRINT_H_

#include <stdint.h>

namespace print
{
	class Print
	{
	public:
		Print() {};
		virtual ~Print() {};

		void print(const char* str) const;
		void printf(const char* format, ...);

		virtual void write(const uint8_t byte) const = 0;
	};
}

#endif /* INCLUDE_PRINT_H_ */

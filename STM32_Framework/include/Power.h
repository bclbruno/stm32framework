/*
 * Power.h
 *
 *  Created on: 27 de mai de 2018
 *      Author: Bruno
 */

#ifndef INCLUDE_POWER_H_
#define INCLUDE_POWER_H_

#include "System.h"

namespace core{
	enum
	{
		REG_ON,	//Regulator ON
		REG_LP,	//Regulator in low power mode
	};

	enum
	{
		ENTRY_WFI,	//Wait for Interrupt
		ENTRY_WFE	//Wait for Event
	};

	void enter_lp_run();
	void enter_sleep_mode(uint32_t regulator, uint8_t entry);
	void enter_stop_mode(uint32_t regulator, uint8_t entry);
	void enter_standby_mode();
}

#endif /* INCLUDE_POWER_H_ */

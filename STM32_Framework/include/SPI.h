/*
 * SPI.h
 *
 *  Created on: 13 de set de 2017
 *      Author: Bruno
 */

#ifndef INCLUDE_SPI_H_
#define INCLUDE_SPI_H_

#include "FWK_def.h"

namespace spi{
	typedef enum
	{
		SPI_STATE_IDLE,
		SPI_STATE_READY,
		SPI_STATE_RECEIVE,
		SPI_STATE_TRANSMIT,
		SPI_STATE_TRANSFER
	}state_t;

	typedef enum
	{
		SPI_1,
		SPI_2,
		SPI_3,
	}device_t;

	typedef enum : uint16_t
	{
		 /** Clock idles low, data captured on rising edge (first transition) */
		SPI_MODE_0,
		/** Clock idles low, data captured on falling edge (second transition) */
		SPI_MODE_1,
		/** Clock idles high, data captured on falling edge (first transition) */
		SPI_MODE_2,
		/** Clock idles high, data captured on rising edge (second transition) */
		SPI_MODE_3,
	}mode_t;

	typedef enum : uint16_t
	{
		SPI_BAUD_PCLK_DIV_2,
		SPI_BAUD_PCLK_DIV_4,
		SPI_BAUD_PCLK_DIV_8,
		SPI_BAUD_PCLK_DIV_16,
		SPI_BAUD_PCLK_DIV_32,
		SPI_BAUD_PCLK_DIV_64,
		SPI_BAUD_PCLK_DIV_128,
		SPI_BAUD_PCLK_DIV_256,
	}baudrate_t;

	typedef enum
	{
		SPI_BIDIMODE	=	0x0001,
		SPI_BIDIOE		=	0x0002,
		SPI_CRCEN		=	0x0004,
		SPI_DFF_8_BIT	=	0x0008,
		SPI_DFF_16_BIT	=	0x0010,
		SPI_RX_ONLY		=	0x0020,
		SPI_SW_SLAVE	=	0x0040,
		SPI_MSB_FRAME	=	0x0100,
		SPI_LSB_FRAME	=	0x0200,
		SPI_MASTER_DEV	=	0x0400,
		SPI_SLAVE_DEV	=	0x0800,
	}flags_t;

	typedef uint16_t config_t;

	class SPI
	{
	public:
		SPI(const device_t instance);
		void init(const mode_t mode, const baudrate_t baudrate, const config_t flags);
		uint8_t  transfer8(const uint8_t byte) const;
		uint16_t transfer16(const uint16_t data) const;

	private:
		void enable_clk() const;
		void config_gpio() const;

		HWRegister	register_;
		device_t	device_;
		state_t		state_;
	};
}



#endif /* INCLUDE_SPI_H_ */

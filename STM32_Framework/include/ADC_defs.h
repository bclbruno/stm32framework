/*
 * adc_defs.h
 *
 *  Created on: 21 de nov de 2017
 *      Author: Bruno
 */

#ifndef INCLUDE_ADC_DEFS_H_
#define INCLUDE_ADC_DEFS_H_

namespace adc
{
	const uint16_t ADC_VALUE_ERROR = 0xFFFF;

	typedef enum
	{
		CHANNEL_0 = 0,
		CHANNEL_1,
		CHANNEL_2,
		CHANNEL_3,
		CHANNEL_4,
		CHANNEL_5,
		CHANNEL_6,
		CHANNEL_7,
		CHANNEL_8,
		CHANNEL_9,
		CHANNEL_10,
		CHANNEL_11,
		CHANNEL_12,
		CHANNEL_13,
		CHANNEL_14,
		CHANNEL_15,
		CHANNEL_16,
		CHANNEL_17,
	}channel_t;

	typedef enum
	{
		ADC_1,
		ADC_2,
		ADC_3,
	}device_t;


}

#endif /* INCLUDE_ADC_DEFS_H_ */

/*
 * LiquidCrystal.h
 *
 *  Created on: 21 de abr de 2018
 *      Author: Bruno
 */

#ifndef LIQUIDCRYSTAL_H_
#define LIQUIDCRYSTAL_H_

#include "Print.h"
#include "GPIO.h"

namespace lcd
{
	const gpio::pin_t unused_pin = {gpio::PORT_A, gpio::PIN_0};

	class LiquidCrystal : public print::Print
	{
	public:
		enum comands : uint8_t
		{
			CLEAR_DISPLAY	= 0x01,
			RETURN_HOME		= 0x02,
			ENTRY_MODE_SET	= 0x04,
			DISPLAY_CONTROL	= 0x08,
			CURSOR_SHIFT	= 0x10,
			FUNCTION_SET	= 0x20,
			SET_CGRAM_ADDR	= 0x40,
			SET_DDRAM_ADDR	= 0x80,
		};

		enum entry_mode : uint8_t
		{
			ENTRY_RIGHT				= 0x00,
			ENTRY_LEFT				= 0x02,
			ENTRY_SHIFT_INCREMENT	= 0x01,
			ENTRY_SHIFT_DECREMENT	= 0x00,
		};

		enum display_control : uint8_t
		{
			DISPLAY_ON	= 0x04,
			DISPLAY_OFF	= 0x00,
			CURSOR_ON	= 0x02,
			CURSOR_OFF	= 0x00,
			BLINK_ON	= 0x01,
			BLINK_OFF	= 0x00,
		};

		enum display_cursor_shift : uint8_t
		{
			DISPLAY_MOVE = 0x08,
			CURSOR_MOVE	 = 0x00,
			MOVE_RIGHT	 = 0x04,
			MOVE_LEFT	 = 0x00,
		};

		enum function_set : uint8_t
		{
			MODE_8BIT	= 0x10,
			MODE_4BIT	= 0x00,
			LINES_2		= 0x08,
			LINES_1		= 0x00,
			DOTS_5x10	= 0x04,
			DOTS_5x8	= 0x00,
		};

	public:
	  LiquidCrystal(const gpio::pin_t rs, const gpio::pin_t enable,
			  const gpio::pin_t d0, const gpio::pin_t d1, const gpio::pin_t d2, const gpio::pin_t d3,
			  const gpio::pin_t d4, const gpio::pin_t d5, const gpio::pin_t d6, const gpio::pin_t d7);
	  LiquidCrystal(const gpio::pin_t rs, const gpio::pin_t rw, const gpio::pin_t enable,
			  const gpio::pin_t d0, const gpio::pin_t d1, const gpio::pin_t d2, const gpio::pin_t d3,
			  const gpio::pin_t d4, const gpio::pin_t d5, const gpio::pin_t d6, const gpio::pin_t d7);
	  LiquidCrystal(const gpio::pin_t rs, const gpio::pin_t rw, const gpio::pin_t enable,
			  const gpio::pin_t d0, const gpio::pin_t d1, const gpio::pin_t d2, const gpio::pin_t d3);
	  LiquidCrystal(const gpio::pin_t rs, const gpio::pin_t enable,
			  const gpio::pin_t d0, const gpio::pin_t d1, const gpio::pin_t d2, const gpio::pin_t d3);

	  void init(const uint8_t cols, const uint8_t rows, const uint8_t charsize = DOTS_5x8);

	  void clear();
	  void home();

	  void noDisplay();
	  void display();
	  void noBlink();
	  void blink();
	  void noCursor();
	  void cursor();
	  void scrollDisplayLeft();
	  void scrollDisplayRight();
	  void leftToRight();
	  void rightToLeft();
	  void autoscroll();
	  void noAutoscroll();

	  void setRowOffsets(const uint8_t row1, const uint8_t row2, const uint8_t row3, const uint8_t row4);
	  void createChar(uint8_t, uint8_t[]);
	  void setCursor(uint8_t, uint8_t);
	  virtual void write(const uint8_t) const;
	  void command(uint8_t);

	private:
	  void init_gpio();

	  void send(const uint8_t, const bool) const;
	  void write4bits(const uint8_t) const;
	  void write8bits(const uint8_t) const;
	  void pulseEnable() const;

	  gpio::GPIO rs_pin_;		// LOW: command.  HIGH: character.
	  gpio::GPIO rw_pin_; 		// LOW: write to LCD.  HIGH: read from LCD.
	  gpio::GPIO enable_pin_;	// activated by a HIGH pulse.
	  gpio::GPIO data_pins_[8];

	  const bool has_rw_pin_;

	  uint8_t displayfunction_;
	  uint8_t displaycontrol_;
	  uint8_t displaymode_;

	  uint8_t numlines_;
	  uint8_t row_offsets_[4];
};

}

#endif /* LIQUIDCRYSTAL_H_ */

/*
 * Power.cpp
 *
 *  Created on: 27 de mai de 2018
 *      Author: Bruno
 */

#include "Power.h"
#include "stm32f10x.h"

using namespace core;

void enter_lp_run()
{

}

void enter_sleep_mode(uint32_t regulator, uint8_t entry)
{
	PWR_EnterSleepMode()
}

void enter_stop_mode(uint32_t regulator, uint8_t entry)
{
	PWR_EnterSTOPMode(regulator, entry);
}

void enter_standby_mode()
{
	PWR_EnterSTANDBYMode();
}

#include "SPI.h"
#include "System.h"
#include "GPIO.h"
#include "SYSTICK.h"
#include "stm32f10x.h"

using namespace spi;

SPI::SPI(const device_t instance):
		device_(instance)
{
	switch(instance)
	{
		case SPI_1: register_ = (HWRegister)SPI1;	break;
		case SPI_2: register_ = (HWRegister)SPI2;	break;
		case SPI_3: register_ = (HWRegister)SPI3;	break;
	}

	state_ = SPI_STATE_IDLE;
}

void SPI::init(const mode_t mode, const baudrate_t baudrate, const config_t flags)
{
	SPI_InitTypeDef config_struct;

	config_struct.SPI_Direction = (flags & SPI_RX_ONLY)   ? SPI_Direction_2Lines_RxOnly : SPI_Direction_2Lines_FullDuplex;
	config_struct.SPI_DataSize  = (flags & SPI_DFF_8_BIT) ? SPI_DataSize_8b : SPI_DataSize_16b;
	config_struct.SPI_FirstBit  = (flags & SPI_MSB_FRAME) ? SPI_FirstBit_MSB : SPI_FirstBit_LSB;
	config_struct.SPI_Mode		= (flags & SPI_MASTER_DEV)? SPI_Mode_Master : SPI_Mode_Slave;
	config_struct.SPI_NSS		= (flags & SPI_SW_SLAVE)  ? SPI_NSS_Soft : SPI_NSS_Hard;
	config_struct.SPI_CRCPolynomial = 0x0007; //Default

	switch(mode)
	{
		case SPI_MODE_0:
		{
			config_struct.SPI_CPHA = SPI_CPHA_1Edge;
			config_struct.SPI_CPOL = SPI_CPOL_Low;
		}
		break;

		case SPI_MODE_1:
		{
			config_struct.SPI_CPHA = SPI_CPHA_2Edge;
			config_struct.SPI_CPOL = SPI_CPOL_Low;
		}
		break;

		case SPI_MODE_2:
		{
			config_struct.SPI_CPHA = SPI_CPHA_1Edge;
			config_struct.SPI_CPOL = SPI_CPOL_High;
		}
		break;

		case SPI_MODE_3:
		{
			config_struct.SPI_CPHA = SPI_CPHA_2Edge;
			config_struct.SPI_CPOL = SPI_CPOL_High;
		}
		break;
	}

	switch(baudrate)
	{
		case SPI_BAUD_PCLK_DIV_2:	config_struct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;	break;
		case SPI_BAUD_PCLK_DIV_4:	config_struct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;	break;
		case SPI_BAUD_PCLK_DIV_8:	config_struct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;	break;
		case SPI_BAUD_PCLK_DIV_16:	config_struct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;	break;
		case SPI_BAUD_PCLK_DIV_32:	config_struct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32;	break;
		case SPI_BAUD_PCLK_DIV_64:	config_struct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_64;	break;
		case SPI_BAUD_PCLK_DIV_128:	config_struct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_128;break;
		case SPI_BAUD_PCLK_DIV_256:	config_struct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;break;
	}

	enable_clk();
	config_gpio();
	SPI_Init((SPI_TypeDef*)register_, &config_struct);
	SPI_Cmd((SPI_TypeDef*)register_, ENABLE);

	state_ = SPI_STATE_READY;
}

void SPI::enable_clk() const
{
	switch(device_)
	{
		case SPI_1: RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);	break;
		case SPI_2: RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);	break;
		case SPI_3:
		{
			//RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3, ENABLE);
		}
		break;
	}
}

void SPI::config_gpio() const
{
	using namespace gpio;

	//TODO: It will configure SPI1 only and no remaped pins
	//TODO: disable any timer using the pin as pwm
	//TODO: It is configuring the pins only for a SPI master
	pin_t sck_pin;
	pin_t miso_pin;
	pin_t mosi_pin;

	pin_def_t sck_def;
	pin_def_t miso_def;
	pin_def_t mosi_def;

	if(device_ == SPI_1)
	{
		sck_def.config	= AF_PUSH_PULL;
		sck_def.speed	= SPEED_50MHZ;
		sck_pin.port	= PORT_A;
		sck_pin.pin_no	= PIN_5;

		miso_def.config	= INPUT_FLOATING;
		miso_def.speed	= SPEED_INPUT;
		miso_pin.port	= PORT_A;
		miso_pin.pin_no	= PIN_6;

		mosi_def.config	= AF_PUSH_PULL;
		mosi_def.speed	= SPEED_50MHZ;
		mosi_pin.port	= PORT_A;
		mosi_pin.pin_no	= PIN_7;
	}

	//GPIOClass will configure the pins
	GPIO(sck_pin.port, sck_pin.pin_no).init(sck_def);
	GPIO(miso_pin.port, miso_pin.pin_no).init(miso_def);
	GPIO(mosi_pin.port, mosi_pin.pin_no).init(mosi_def);
}

uint8_t SPI::transfer8(const uint8_t data) const
{
	return static_cast<uint8_t>(transfer16(static_cast<uint16_t>(data)));
}

uint16_t SPI::transfer16(const uint16_t data) const
{
	// read any previous data
	SPI_I2S_ReceiveData((SPI_TypeDef*)register_);
	// Write the data item to be transmitted into the SPI_DR register
	SPI_I2S_SendData((SPI_TypeDef*)register_, data);
	// Wait until TX buffer is empty
	while (SPI_I2S_GetFlagStatus((SPI_TypeDef*)register_, SPI_I2S_FLAG_TXE) == RESET);
	// Wait while SPI is communicating
	while (SPI_I2S_GetFlagStatus((SPI_TypeDef*)register_, SPI_I2S_FLAG_BSY));
	// Read received data
	return SPI_I2S_ReceiveData((SPI_TypeDef*)register_);
}

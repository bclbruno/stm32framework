/*
 * Flash.cpp
 *
 *  Created on: 26 de abr de 2018
 *      Author: Bruno
 */

#include "Flash.h"
#include "stm32f10x.h"

void core::flash::set_latency(latency_t latency)
{
	FLASH->ACR &= ~ACR_LATENCY_MASK;
	FLASH->ACR |= latency;
}

void core::flash::enable_prefetch()
{
	FLASH->ACR |= ACR_PREFETCH_BUFFER_BIT;
}

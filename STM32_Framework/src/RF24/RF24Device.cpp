/*
 * CRF24.cpp
 *
 *  Created on: 18 de set de 2017
 *      Author: Bruno
 */

#include "RF24/RF24Device.h"
#include <algorithm>
#include "SYSTICK.h"
#include "System.h"

using namespace rf24;

#ifdef RF24_SERIAL_DEBUG
serial::CSerial* rf24_serial = NULL;
#endif

namespace{
	const uint8_t MAX_PAYLOAD_SIZE = 32;
}

RF24Device::RF24Device(const gpio::pin_t csn_pin, const gpio::pin_t ce_pin, spi::SPI& spi):
		csn_pin_(csn_pin.port, csn_pin.pin_no),
		ce_pin_(ce_pin.port, ce_pin.pin_no),
		spi_(spi),
		payload_size_(MAX_PAYLOAD_SIZE),
		dynamic_payloads_enabled_(false)
{
	pipe0_reading_address_ = 0;
}

uint8_t RF24Device::init()
{
	csn_pin_.init(gpio::OUTPUT_PUSH_PULL, gpio::SPEED_50MHZ);
	ce_pin_.init(gpio::OUTPUT_PUSH_PULL, gpio::SPEED_50MHZ);

	ce_pin_.set_low();
	csn_pin_.set_high();

	// Must allow the radio time to settle else configuration bits will not necessarily stick.
	// This is actually only required following power up but some settling time also appears to
	// be required after resets too. For full coverage, we'll always assume the worst.
	// Enabling 16b CRC is by far the most obvious case if the wrong timing is used - or skipped.
	// Technically we require 4.5ms + 14us as a worst case. We'll just call it 5ms for good measure.
	// WARNING: Delay is based on P-variant whereby non-P *may* require different timing.
	core::delay(5);
	write_register(NRF_CONFIG, 0x00);

	//Set 1500uS (minimum for 32B payload in ESB@250KBPS) timeouts, to make testing a little easier
	write_register(SETUP_RETR, SETUP_RETR_ARD_1500us | SETUP_RETR_ARC_15RETRY);

	// Set RF output power to max
	set_PA_level(RF_SETUP_PA_MAX);

	set_data_rate(RF_SETUP_DR_1MBPS);

	// Initialize CRC and request 2-byte (16bit) CRC
	set_crc_length(RF24_CRC_16);

	// Disable dynamic payloads, to match dynamic_payloads_enabled setting
	write_register(DYNPD, 0);

	//Clear RX_DR, TX_DS and MAX_RT interrupts
	write_register(NRF_STATUS, RX_DR | TX_DS | MAX_RT);

	set_channel(76);

	flush_rx();
	flush_tx();

	return read_register(NRF_CONFIG);
}

uint8_t RF24Device::read_register(const uint8_t reg) const
{
	csn_pin_.set_low();
	spi_.transfer8(R_REGISTER | reg);
	const uint8_t result = spi_.transfer8(NOP);
	csn_pin_.set_high();

	return result;
}

uint8_t RF24Device::read_register(const uint8_t reg, void* buffer, uint8_t buffer_length) const
{
	uint8_t* buffer_byte = reinterpret_cast<uint8_t*>(buffer);

	csn_pin_.set_low();
	const uint8_t status = spi_.transfer8(R_REGISTER | reg);
	while(buffer_length--)
	{
		*buffer_byte++ = spi_.transfer8(NOP);
	}
	csn_pin_.set_high();

	return status;
}

uint8_t RF24Device::read_payload(void* buffer, const uint8_t buffer_length) const
{
	uint8_t* buffer_byte = reinterpret_cast<uint8_t*>(buffer);

	uint8_t data_length  = std::min(buffer_length, payload_size_);
	uint8_t blank_length = dynamic_payloads_enabled_ ? 0 : (uint8_t)(payload_size_ - data_length);

	csn_pin_.set_low();
	const uint8_t status = spi_.transfer8(R_RX_PAYLOAD);
	while ( data_length-- )
	{
		*buffer_byte++ = spi_.transfer8(NOP);
	}
	while ( blank_length-- )
	{
		spi_.transfer8(NOP);
	}
	csn_pin_.set_high();

	return status;
}

bool RF24Device::read(void* buffer, const uint8_t buffer_length) const
{
	read_payload(buffer, buffer_length);

	const bool rx_empty = (read_register(FIFO_STATUS) & FIFO_RX_EMPTY);
	if(rx_empty)
	{
		//Clear RX_DR flag
		write_register(NRF_STATUS, RX_DR );
	}

	return rx_empty;
}

uint8_t RF24Device::write_register(const uint8_t reg, const uint8_t value) const
{
	csn_pin_.set_low();
	const uint8_t result = spi_.transfer8(W_REGISTER | reg);
	spi_.transfer8(value);
	csn_pin_.set_high();

	return result;
}

uint8_t RF24Device::write_register(const uint8_t reg, const void* buffer, uint8_t buffer_length) const
{
	const uint8_t* buffer_byte = reinterpret_cast<const uint8_t*>(buffer);

	csn_pin_.set_low();
	const uint8_t status = spi_.transfer8( W_REGISTER | reg );
	while ( buffer_length-- )
	{
	  spi_.transfer8(*buffer_byte++);
	}
	csn_pin_.set_high();

	return status;
}

uint8_t RF24Device::write_payload(const void* buffer, const uint8_t buffer_length) const
{
	const uint8_t* buffer_byte = reinterpret_cast<const uint8_t*>(buffer);

	uint8_t data_length  = std::min(buffer_length, payload_size_);
	uint8_t blank_length = dynamic_payloads_enabled_ ? 0 : (uint8_t)(payload_size_ - data_length);

	csn_pin_.set_low();
	const uint8_t status = spi_.transfer8(W_TX_PAYLOAD);
	while(data_length--)
	{
		spi_.transfer8(*buffer_byte++);
	}
	while(blank_length--)
	{
		spi_.transfer8(0x00);
	}
	csn_pin_.set_high();

	return status;
}

uint8_t RF24Device::write(const void* buffer, const uint8_t buffer_length) const
{
	uint8_t ret;
	// Send the payload
	write_payload(buffer, buffer_length);

	ce_pin_.set_high();
	core::delay_us(15);
	ce_pin_.set_low();

	const uint32_t timeout = 200;
	const uint32_t initial_time = core::millis();
	uint8_t status = 0x00;
	while( !(status & (TX_DS | MAX_RT)) )
	{
		if((core::millis() - initial_time) > timeout)
		{
			break;
		}
		status = get_status();
	}

	//Clear TX_DS and MAX_RT flags
	write_register(NRF_STATUS, TX_DS | MAX_RT);

	if(status & TX_DS)
	{
		ret = MSG_SENT;
	}
	else if(status & MAX_RT)
	{
		ret = MSG_TIMEOUT;
	}
	else
	{
		ret = SPI_TIMEOUT;
	}

	return ret;
}

uint8_t RF24Device::spi_transfer(const uint8_t cmd) const
{
	csn_pin_.set_low();
	const uint8_t status = spi_.transfer8(cmd);
	csn_pin_.set_high();

	return status;
}

void RF24Device::set_PA_level(const PA_LEVEL level) const
{
	uint8_t setup = read_register(RF_SETUP);
	setup &= RF_SETUP_PA_NOT_MASK;
	setup |= level;

	write_register(RF_SETUP, setup);
}

void RF24Device::set_channel(const uint8_t channel) const
{
	const uint8_t max_channel = 127;
	write_register(RF_CH, std::min(channel, max_channel));
}

void RF24Device::set_crc_length(const uint8_t length) const
{
	uint8_t config = read_register(NRF_CONFIG);
	config &= RF24_CRC_NOT_MASK;
	config |= length;
	write_register(NRF_CONFIG, config);
}

void RF24Device::start_listening() const
{
	const uint8_t config = read_register(NRF_CONFIG);
	write_register(NRF_CONFIG, config | PRIM_RX);

	//Clear RX_DR, TX_DS and MAX_RT interrupts
	write_register(NRF_STATUS, RX_DR | TX_DS | MAX_RT);

	if(pipe0_reading_address_)
	{
		write_register(RX_ADDR_P0, &pipe0_reading_address_, 5);

#ifdef RF24_SERIAL_DEBUG
		if(rf24_serial)
		{
			uint64_t wrote_addr = 0;
			read_register(RX_ADDR_P0, &wrote_addr, 5);
			rf24_serial->print("SL wrote PIPE0: ");
			rf24_serial->print(uint64_t(wrote_addr));
			rf24_serial->println();
		}
#endif
	}

	//Flush any previous data
	flush_rx();
	flush_tx();

	//Set the enable pin
	ce_pin_.set_high();

	//130us settling time
	core::delay_us(150);
}

void RF24Device::stop_listening() const
{
	ce_pin_.set_low();
	flush_tx();
	flush_rx();

	const uint8_t cfg = read_register(NRF_CONFIG);
	write_register(NRF_CONFIG, cfg & PRIM_TX);

	//130us settling time
	core::delay_us(150);
}

bool RF24Device::set_data_rate(const DATA_RATE data_rate) const
{
	uint8_t setup = read_register(RF_SETUP);
	setup &= RF_SETUP_DR_NOT_MASK;
	setup |= data_rate;

	write_register(RF_SETUP, setup);

	return (read_register(RF_SETUP) == setup);
}

void RF24Device::set_payload_size(const uint8_t size)
{
	payload_size_ = std::min(size, MAX_PAYLOAD_SIZE);
}

bool RF24Device::available() const
{
	return get_status() & RX_DR;
}

uint8_t RF24Device::get_status() const
{
	csn_pin_.set_low();
	const uint8_t status = spi_.transfer8(NOP);
	csn_pin_.set_high();

	return status;
}

void RF24Device::open_writing_pipe(const PipeAddress address) const
{
	write_register(RX_ADDR_P0, &address, 5);
	write_register(RX_PW_P0, payload_size_);
	write_register(TX_ADDR,    &address, 5);
}

void RF24Device::open_reading_pipe(const uint8_t child, const PipeAddress address)
{
	// If this is pipe 0, cache the address.  This is needed because
	// openWritingPipe() will overwrite the pipe 0 address, so
	// startListening() will have to restore it.
	if (child == 0)
	{
		pipe0_reading_address_ = address;
	}

	if (child < 6)
	{
		const uint8_t pipe_register = static_cast<uint8_t>(RX_ADDR_P0 + child);
		const uint8_t payload_size_reg = static_cast<uint8_t>(RX_PW_P0 + child);

		// For pipes 2-5, only write the LSB
		if ( child < 2 )
		{
			write_register(pipe_register, &address, 5);
		}
		else
		{
			write_register(pipe_register, &address, 1);
		}

#ifdef RF24_SERIAL_DEBUG
		if(rf24_serial)
		{
			uint64_t wrote_addr = 0;
			read_register(pipe_register, &wrote_addr, 5);
			rf24_serial->print("OR tried to write ");
			rf24_serial->print(address);
			rf24_serial->print(" on PIPE");
			rf24_serial->print(child);
			rf24_serial->println();
			rf24_serial->print("OR wrote PIPE");
			rf24_serial->print(child);
			rf24_serial->print(": ");
			rf24_serial->print(uint64_t(wrote_addr));
			rf24_serial->println();
		}
#endif

		write_register(payload_size_reg, payload_size_);

		// Note it would be more efficient to set all of the bits for all open
		// pipes at once.  However, I thought it would make the calling code
		// more simple to do it this way.
		const uint8_t enable_reg_value = read_register(EN_RXADDR) | uint8_t((uint8_t(0x01)<<child));
		write_register(EN_RXADDR, enable_reg_value);

#ifdef RF24_SERIAL_DEBUG
		if(rf24_serial)
		{
			rf24_serial->print("OR EN_RXADDR ");
			rf24_serial->print(read_register(EN_RXADDR));
			rf24_serial->println();
		}
#endif

	}
}

void RF24Device::close_reading_pipe(const uint8_t pipe) const
{
	const uint8_t enable_reg_value = read_register(EN_RXADDR) | uint8_t(~(uint8_t(0x01)<<pipe));
	write_register(EN_RXADDR, enable_reg_value);
}

void RF24Device::power_up() const
{
	const uint8_t cfg = read_register(NRF_CONFIG);
	write_register(NRF_CONFIG, cfg | PWR_UP);
	//1.5ms power up time
	core::delay(2);
}

void RF24Device::power_down() const
{
	const uint8_t cfg = read_register(NRF_CONFIG);
	write_register(NRF_CONFIG, cfg & PWR_DOWN);
}

void RF24Device::set_retries(const SETUP_RETR_BITS retries_flag) const
{
	write_register(SETUP_RETR, retries_flag);
}

/*
 * RF24Network.cpp
 *
 *  Created on: 26 de out de 2017
 *      Author: Bruno
 */

#include "RF24/RF24Network.h"

using namespace rf24;

#ifdef RF24_NET_SERIAL_DEBUG
serial::CSerial* rf24_serial = NULL;
#endif


RF24Network::RF24Network(RF24Device& radio):
		radio_device_(radio)
{
	node_address_ = 0;
}

bool RF24Network::init(const NodeAddress node)
{
	if(!is_valid_address(node))
	{
		return false;
	}

	radio_device_.init();
	radio_device_.set_PA_level(RF_SETUP_PA_MAX);
	radio_device_.set_payload_size(sizeof(RF24Frame));

	set_node_address(node);

	return true;
}

void RF24Network::set_node_address(const NodeAddress node)
{
	node_address_ = node;

	//Assign different delays to reduce data collision
	//Min retry time per node will be 15 * 3000us = 45ms
	//Max retry time per node will be 15 * 4000us = 60ms
	const uint8_t         retry_period = static_cast<uint8_t>((node_address_ % 5) + SETUP_RETR_ARD_3000us);
	const SETUP_RETR_BITS retries_flag = static_cast<SETUP_RETR_BITS>(retry_period | SETUP_RETR_ARC_15RETRY);

	radio_device_.set_retries( retries_flag );

	//Open children pipe and store it
	for(uint8_t child = 0; child < 6; ++child)
	{
		children_pipe_[child] = children_pipe(child);
		radio_device_.open_reading_pipe(child, children_pipe_[child]);
	}
}

MessageType RF24Network::update(RF24Frame* message)
{
	MessageType status = NETWORK_IDLE;

	if(radio_device_.available())
	{
		radio_device_.read(message, sizeof(RF24Frame));

		if(message->to_node == node_address_)
		{
			status = process_message(message);
		}
		else
		{
			forward_message(message);
		}
	}

	return status;
}

MessageType RF24Network::process_message(RF24Frame* message) const
{
	MessageType status = NETWORK_IDLE;

	switch(message->type)
	{
		case NETWORK_PING:
		{
			message->to_node	= message->from_node;
			message->from_node	= node_address_;
			message->type		= NETWORK_PONG;

			send_message(message);
		}
		break;

		default:
		{
			status = message->type;
		}
	}

	return status;
}

void RF24Network::forward_message(RF24Frame* message) const
{
	send_message(message);
}

bool RF24Network::is_valid_address(NodeAddress node)
{
	bool result = true;

	if(node != MASTER_ADDRESS)
	{
		while(node)
		{
			const uint8_t digit = static_cast<uint8_t>(node & 0x07);
			node = static_cast<NodeAddress>(node >> 3);

			if(digit < 1 || digit > 5)
			{
				result = false;
				break;
			}
		}
	}

	return result;
}

uint8_t RF24Network::send_message(RF24Frame* message) const
{
	uint8_t status = INVALID_ADDRESS;

	if( message->to_node != node_address_	&&
		is_valid_address(message->to_node)	)
	{
		const uint8_t child = get_message_destiny(message->to_node);

		radio_device_.stop_listening();

		radio_device_.open_writing_pipe(children_pipe_[child]);
		status = radio_device_.write(message, sizeof(RF24Frame));

		if( status == MSG_TIMEOUT				&&
			is_ack_type(message->type)			&&
			message->from_node != node_address_	)
		{
			send_nack(message);
		}

		radio_device_.start_listening();
	}

	return status;
}

void RF24Network::send_nack(RF24Frame* message) const
{
	RF24Frame nack_msg;
	nack_msg.from_node	= node_address_;
	nack_msg.to_node	= message->from_node;
	nack_msg.id			= message->id;
	nack_msg.type		= rf24::NETWORK_NACK;
	nack_msg.data		= message->to_node;

	send_message(&nack_msg);
}

bool RF24Network::is_ack_type(MessageType type) const
{
	return (type > NETWORK_IS_ACK_BOTTOM) && (type < NETWORK_IS_ACK_TOP);
}

PipeAddress RF24Network::children_pipe(const uint8_t child) const
{
	NodeAddress child_address;
	PipeAddress child_pipe = MASTER_PIPE;

	if((node_address_ != MASTER_ADDRESS) || (child != 0))
	{
		if(child == 0)
		{
			child_address = node_address_;
		}
		else
		{
			child_address = static_cast<NodeAddress>((node_address_ << 3) + child);
		}

		PipeAddress child_mask = child_address;
		while(child_mask)
		{
			child_mask = static_cast<PipeAddress>(child_mask >> 3);
			child_pipe = static_cast<PipeAddress>(child_pipe << 3);
		}

		child_pipe = child_pipe | static_cast<PipeAddress>(child_address);
		child_pipe = child_pipe & MASTER_PIPE;
	}

#ifdef RF24_NET_SERIAL_DEBUG
	if(rf24_serial)
	{
		rf24_serial->print("child ");
		rf24_serial->print(child);
		rf24_serial->print(" pipe address: ");
		rf24_serial->print(child_pipe);
		rf24_serial->println();

	}
#endif

	return child_pipe;
}

uint8_t RF24Network::get_message_destiny(NodeAddress node) const
{
	uint8_t destiny = 0;

	while(node)
	{
		destiny = static_cast<uint8_t>(node & 0x07);
		node = static_cast<NodeAddress>(node >> 3);
		if(node == node_address_)
		{
			break;
		}
	}

	if( (node == 0) && (node_address_ != MASTER_ADDRESS) )
	{
		destiny = 0;
	}

#ifdef RF24_NET_SERIAL_DEBUG
	if(rf24_serial)
	{
		rf24_serial->print("msg_destiny: ");
		rf24_serial->print(destiny);
		rf24_serial->println();
	}
#endif

	return destiny;
}

void RF24Network::power_up()
{
	radio_device_.power_up();
	radio_device_.start_listening();
}

void RF24Network::power_down()
{
	radio_device_.stop_listening();
	radio_device_.power_down();
}

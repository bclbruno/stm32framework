/*
 * EXTI.cpp
 *
 *  Created on: 1 de out de 2017
 *      Author: Bruno
 */

#include "EXTI.h"
#include <stddef.h>
#include "stm32f10x.h"

#ifndef __always_inline
	#define __always_inline __attribute__((always_inline))
#endif

#ifndef __unused
	#define __unused __attribute__((unused))
#endif

using namespace exti;

static inline void dispatch_single_exti(uint32_t exti_num);
static inline void dispatch_extis(uint32_t start, uint32_t stop);

static voidFuncPtr exti_callbacks[] =
{
    NULL,  // EXTI0
    NULL,  // EXTI1
    NULL,  // EXTI2
	NULL,  // EXTI3
	NULL,  // EXTI4
	NULL,  // EXTI5
	NULL,  // EXTI6
	NULL,  // EXTI7
	NULL,  // EXTI8
	NULL,  // EXTI9
	NULL,  // EXTI10
	NULL,  // EXTI11
	NULL,  // EXTI12
	NULL,  // EXTI13
	NULL,  // EXTI14
	NULL,  // EXTI15
};

static voidPtr exti_parameter[] =
{
    NULL,  // EXTI0
    NULL,  // EXTI1
    NULL,  // EXTI2
	NULL,  // EXTI3
	NULL,  // EXTI4
	NULL,  // EXTI5
	NULL,  // EXTI6
	NULL,  // EXTI7
	NULL,  // EXTI8
	NULL,  // EXTI9
	NULL,  // EXTI10
	NULL,  // EXTI11
	NULL,  // EXTI12
	NULL,  // EXTI13
	NULL,  // EXTI14
	NULL,  // EXTI15
};


CEXTI::CEXTI(const gpio::port_t port, const gpio::pin_no_t pin_no):
		int_pin_(port, pin_no)
{
	line_ = 0;
	if(pin_no != gpio::PIN_ALL)
	{
		line_ = static_cast<uint32_t>(pin_no);
	}

}
void CEXTI::configure_pin(const gpio::pin_config_t config, const gpio::pin_speed_t mode)
{
	int_pin_.init(config, mode);
	int_pin_.enable_ext_int();
}

void CEXTI::attach_callback(voidFuncPtr callback, voidPtr parameter) const
{
	exti_callbacks[line_] = callback;
	exti_parameter[line_] = parameter;
}

void CEXTI::deattach_callback() const
{
	exti_callbacks[line_] = NULL;
	exti_parameter[line_] = NULL;
}

void CEXTI::enable_interrupt() const
{
	EXTI->IMR |= (0x1U << line_);

	switch(line_)
	{
		case EXTI0: NVIC_EnableIRQ(EXTI0_IRQn);	break;
		case EXTI1: NVIC_EnableIRQ(EXTI1_IRQn);	break;
		case EXTI2: NVIC_EnableIRQ(EXTI2_IRQn);	break;
		case EXTI3: NVIC_EnableIRQ(EXTI3_IRQn);	break;
		case EXTI4: NVIC_EnableIRQ(EXTI4_IRQn);	break;
		default:
		{
			if(line_ < EXTI10)
			{
				NVIC_EnableIRQ(EXTI9_5_IRQn);
			}
			else if(line_ <= EXTI15)
			{
				NVIC_EnableIRQ(EXTI15_10_IRQn);
			}
		}
	}
}

void CEXTI::disable_interrupt() const
{
	EXTI->IMR &= ~(0x1U << line_);
}

void CEXTI::enable_event() const
{
	EXTI->EMR |= (0x1U << line_);
}

void CEXTI::disable_event() const
{
	EXTI->EMR &= ~(0x1U << line_);
}

void CEXTI::trigger_selection(const EXTI_trigger trigger) const
{
	if(trigger == EXTI_NONE_EDGE)
	{
		EXTI->RTSR &= ~(0x1U << line_);
		EXTI->FTSR &= ~(0x1U << line_);
	}
	else
	{
		if( (trigger == EXTI_FALLING_EDGE) || (trigger == EXTI_BOTH_EDGE) )
		{
			EXTI->FTSR |= (0x1U << line_);
		}
		if( (trigger == EXTI_RISING_EDGE) || (trigger == EXTI_BOTH_EDGE) )
		{
			EXTI->RTSR |= (0x1U << line_);
		}
	}
}

/*
 * Low level functions
 */

void __irq_exti0(void)
{
    dispatch_single_exti(EXTI0);
}

void __irq_exti1(void)
{
    dispatch_single_exti(EXTI1);
}

void __irq_exti2(void)
{
    dispatch_single_exti(EXTI2);
}

void __irq_exti3(void)
{
    dispatch_single_exti(EXTI3);
}

void __irq_exti4(void)
{
    dispatch_single_exti(EXTI4);
}

void __irq_exti9_5(void)
{
    dispatch_extis(EXTI5, EXTI9);
}

void __irq_exti15_10(void)
{
    dispatch_extis(EXTI10, EXTI15);
}


/*
 * Auxiliary functions
 */

/* Clear the pending bits for EXTIs whose bits are set in exti_msk.
 *
 * If a pending bit is cleared as the last instruction in an ISR, it
 * won't actually be cleared in time and the ISR will fire again.  To
 * compensate, this function NOPs for 2 cycles after clearing the
 * pending bits to ensure it takes effect. */
static inline __always_inline void clear_pending_msk(const uint32_t exti_msk)
{
    EXTI->PR = exti_msk;
    asm volatile("nop");
    asm volatile("nop");
}

/* This dispatch routine is for non-multiplexed EXTI lines only; i.e.,
 * it doesn't check EXTI_PR. */
static inline __always_inline void dispatch_single_exti(const uint32_t exti)
{
	const voidFuncPtr handler = exti_callbacks[exti];
	const voidPtr parameter = exti_parameter[exti];

    if (handler)
    {
    	handler(parameter);
    }

    clear_pending_msk(1U << exti);
}

/* Dispatch routine for EXTIs which share an IRQ. */
static inline __always_inline void dispatch_extis(const uint32_t start, const uint32_t stop)
{
	const uint32_t pr = EXTI->PR;
	uint32_t handled_msk = 0;
	uint32_t exti;

    /* Dispatch user handlers for pending EXTIs. */
    for (exti = start; exti <= stop; ++exti)
    {
    	uint32_t eb = (1U << exti);
        if (pr & eb)
        {
        	const voidFuncPtr handler = exti_callbacks[exti];
        	const voidPtr parameter = exti_parameter[exti];

            if (handler)
            {
                handler(parameter);
                handled_msk |= eb;
            }
        }
    }

    /* Clear the pending bits for handled EXTIs. */
    clear_pending_msk(handled_msk);
}

/*
 * SYSTICK.cpp
 *
 *  Created on: 16 de set de 2017
 *      Author: Bruno
 */

#include "SYSTICK.h"
#include "stm32f10x.h"

volatile uint32_t systick_uptime_millis;

void core::delay(const uint32_t ms)
{
    const uint32_t start = systick_uptime_millis;
    while (systick_uptime_millis - start < ms);
}

uint32_t core::millis()
{
	return systick_uptime_millis;
}

/**
 * @brief  Initialize and start the SysTick counter and its interrupt.
 *
 * @param   ticks   number of ticks between two interrupts
 * @return  void
 *
 * Initialise the system tick timer and its interrupt and start the
 * system tick timer / counter in free running mode to generate
 * periodical interrupts.
 */
void core::config_systick(const uint32_t ticks)
{
	SysTick_Config(ticks);
}


/* Low level interrupt */
void SysTick_Handler(void)
{
	++systick_uptime_millis;
}

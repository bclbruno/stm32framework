#include <GPIO.h>
#include "System.h"
#include "stm32f10x.h"

using namespace gpio;

/**
  * @brief  Creates a GPIO object
  * @param  port: Pin port
  *   This parameter can be one of the following values:
  *     @arg PORT_A
  *     @arg PORT_B
  *     @arg PORT_C
  *     @arg PORT_D
  *     @arg PORT_E
  *     @arg PORT_F
  *     @arg PORT_G
  * @param  pin_no: Pin number
  *   This parameter can be one of the following values:
  *     @arg PIN_0
  *     @arg PIN_1
  *     @arg PIN_2
  *     @arg PIN_3
  *     @arg PIN_4
  *     @arg PIN_5
  *     @arg PIN_6
  *     @arg PIN_7
  *     @arg PIN_8
  *     @arg PIN_9
  *     @arg PIN_10
  *     @arg PIN_11
  *     @arg PIN_12
  *     @arg PIN_13
  *     @arg PIN_14
  *     @arg PIN_15
  * @retval None
  */
GPIO::GPIO(const port_t port, const pin_no_t pin_no):
		pin_mask_((uint32_t)0x01 << pin_no),
		pin_no_(pin_no), port_(port)
{
	switch(port)
	{
		case PORT_A: 	register_ = (HWRegister)GPIOA; 	break;
		case PORT_B: 	register_ = (HWRegister)GPIOB; 	break;
		case PORT_C: 	register_ = (HWRegister)GPIOC; 	break;
		case PORT_D: 	register_ = (HWRegister)GPIOD; 	break;
		case PORT_E: 	register_ = (HWRegister)GPIOE; 	break;
		case PORT_F: 	register_ = (HWRegister)GPIOF; 	break;
		case PORT_G: 	register_ = (HWRegister)GPIOG; 	break;
	}
}

/**
  * @brief  Initializes GPIO clock and configuration
  * @param  config: Pin configuration
  *   This parameter can be one of the following values:
  *     @arg INPUT_FLOATING: General propose input pin with high impedance
  *     @arg INPUT_PULL_UP: General propose input pin with weak pull-up resistor
  *     @arg INPUT_PULL_DOWN: General propose input pin with weak pull-down resistor
  *     @arg ANALOG: Analog input
  *     @arg OUTPUT_OPEN_DRAIN: General propose output in open drain configuration
  *     @arg OUTPUT_PUSH_PULL: General propose output in push-pull configuration
  *     @arg AF_OPEN_DRAIN: Alternate function output in open drain configuration
  *     @arg AF_PUSH_PULL: Alternate function output in push-pull configuration
  * @param  speed: specifies the output refreshing rate of the pin
  *   This parameter can be one of the following values:
  *     @arg SPEED_2MHZ:  Set pin clk to 2Mhz
  *     @arg SPEED_10MHZ: Set pin clk to 10Mhz
  *     @arg SPEED_50MHZ: Set pin clk to 50Mhz
  *     @arg SPEED_INPUT: Input pin
  * @retval None
  */
void GPIO::init(const pin_config_t config, const pin_speed_t speed)
{
	GPIO_InitTypeDef init_struct;

	enable_clock(port_);

	init_struct.GPIO_Pin   = pin_mask_;
	init_struct.GPIO_Mode  = (GPIOMode_TypeDef)get_mode(config);
	init_struct.GPIO_Speed = (GPIOSpeed_TypeDef)get_speed(speed);

	GPIO_Init((GPIO_TypeDef*)register_, &init_struct);

	if(config == INPUT_PULL_UP)
	{
		set_high();
	}
	else
	{
		set_low();
	}
}

/**
  * @brief  Enables the external interrupt
  * @param  None
  * @retval None
  */
void GPIO::enable_ext_int() const
{
//	core::enable_APB2ENR(RCC_APB2ENR_AFIOEN);
	GPIO_EXTILineConfig(port_, pin_no_);
}

/**
  * @brief  Sets GPIO output high.
  * @param  None
  * @retval None
  */
void GPIO::set_high() const
{
	GPIO_SetBits((GPIO_TypeDef*)register_, pin_mask_);
}

/**
  * @brief  Sets GPIO output low.
  * @param  None
  * @retval None
  */
void GPIO::set_low() const
{
	GPIO_ResetBits((GPIO_TypeDef*)register_, pin_mask_);
}

/**
  * @brief  Write into GPIO output.
  * @param  state: New GPIO state true/false
  * @retval None
  */
void GPIO::write(const bool state) const
{
	if(state)
	{
		set_high();
	}
	else
	{
		set_low();
	}
}

/**
  * @brief  Read GPIO state
  * @param  None
  * @retval Returns true if GPIO state is high, false otherwise
  */
bool GPIO::read() const
{
	return GPIO_ReadInputDataBit((GPIO_TypeDef*)register_, pin_mask_);
}

/**
  * @brief  Enables GPIO Port clock
  * @param  port: GPIO Port which clock will be enabled
  * @retval None
  */
void GPIO::enable_clock(const port_t port)
{
#if defined (STM32F10X_LD) || defined (STM32F10X_LD_VL) || defined (STM32F10X_MD) || defined (STM32F10X_MD_VL) || defined (STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined (STM32F10X_XL) || defined (STM32F10X_CL)
	switch(port)
	{
		case PORT_A: 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	break;
		case PORT_B: 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);	break;
		case PORT_C: 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);	break;
		case PORT_D: 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);	break;
		case PORT_E: 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);	break;
		case PORT_F: 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF, ENABLE);	break;
		case PORT_G: 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOG, ENABLE);	break;
	}
#endif
}

/**
  * @brief  Transforms High Level pin configuration into Low Level (processor dependent) pin configuration
  * @param  config: HL pin configuration
  * @retval LL pin configuration
  */
uint8_t GPIO::get_mode(const pin_config_t config) const
{
#if defined (STM32F10X_LD) || defined (STM32F10X_LD_VL) || defined (STM32F10X_MD) || defined (STM32F10X_MD_VL) || defined (STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined (STM32F10X_XL) || defined (STM32F10X_CL)
	switch(config)
	{
		case INPUT_FLOATING:	return GPIO_Mode_IN_FLOATING;
		case INPUT_PULL_UP:		return GPIO_Mode_IPU;
		case INPUT_PULL_DOWN:	return GPIO_Mode_IPD;
		case ANALOG:			return GPIO_Mode_AIN;
		case OUTPUT_OPEN_DRAIN:	return GPIO_Mode_Out_OD;
		case OUTPUT_PUSH_PULL:	return GPIO_Mode_Out_PP;
		case AF_OPEN_DRAIN:		return GPIO_Mode_AF_OD;
		case AF_PUSH_PULL:		return GPIO_Mode_AF_PP;
		default:				return GPIO_Mode_IN_FLOATING;
	}
#endif
}

/**
  * @brief  Transforms High Level pin speed into Low Level (processor dependent) pin speed
  * @param  config: HL pin speed
  * @retval LL pin speed
  */
uint8_t GPIO::get_speed(const pin_speed_t speed) const
{
#if defined (STM32F10X_LD) || defined (STM32F10X_LD_VL) || defined (STM32F10X_MD) || defined (STM32F10X_MD_VL) || defined (STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined (STM32F10X_XL) || defined (STM32F10X_CL)
	switch(speed)
	{
		case SPEED_2MHZ:	return GPIO_Speed_2MHz;
		case SPEED_10MHZ:	return GPIO_Speed_10MHz;
		case SPEED_50MHZ:
		case SPEED_INPUT:
		default:			return GPIO_Speed_50MHz;
	}
#endif
}

/*
 * Serial.cpp
 *
 *  Created on: 6 de out de 2017
 *      Author: Bruno
 */

#include "Serial.h"
#include "stm32f10x.h"
#include "GPIO.h"
#include "System.h"

using namespace serial;

#define WORD_MASK		0x0F
#define PARITY_MASK		0x30
#define STOP_BIT_MASK	0xC0

static Serial* serial_callbacks[] =
{
	NULL,	//USART1
	NULL,	//USART2
	NULL,	//USART3
};

Serial::Serial(const device_t device):
		device_(device)
{
	switch(device)
	{
		case SERIAL1:
		{
			register_ = (HWRegister)USART1;
			serial_callbacks[0] = this;
		}
		break;

		case SERIAL2:
		{
			register_ = (HWRegister)USART2;
			serial_callbacks[1] = this;
		}
		break;

		case SERIAL3:
		{
			register_ = (HWRegister)USART3;
			serial_callbacks[2] = this;
		}
		break;
	}

	rx_buffer_.clear();
}

void Serial::init(const uint32_t baud_rate, const uint16_t config) const
{
	USART_InitTypeDef init_struct;

	init_struct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	init_struct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	init_struct.USART_BaudRate = baud_rate;
	init_struct.USART_WordLength = config & WORD_MASK;

	switch(config & PARITY_MASK)
	{
		case PARITY_NONE: init_struct.USART_Parity = USART_Parity_No;	break;
		case PARITY_EVEN: init_struct.USART_Parity = USART_Parity_Even;	break;
		case PARITY_ODD:  init_struct.USART_Parity = USART_Parity_Odd;	break;
	}

	switch(config & STOP_BIT_MASK)
	{
		case STOP_BIT_1:	init_struct.USART_StopBits = USART_StopBits_1;		break;
		case STOP_BIT_0_5:	init_struct.USART_StopBits = USART_StopBits_0_5;	break;
		case STOP_BIT_2:	init_struct.USART_StopBits = USART_StopBits_2;		break;
		case STOP_BIT_1_5:	init_struct.USART_StopBits = USART_StopBits_1_5;	break;
	}

	enable_clk();
	init_gpio();
	enable_interrupt();

	USART_Init((USART_TypeDef*)register_, &init_struct);
	USART_ITConfig((USART_TypeDef*)register_, USART_IT_RXNE, ENABLE);
	USART_Cmd((USART_TypeDef*)register_, ENABLE);
}

void Serial::enable_clk() const
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

	switch(device_)
	{
		case SERIAL1:
		{
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
		}
		break;

		case SERIAL2:
		{
			RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
		}
		break;

		case SERIAL3:
		{
			RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
		}
		break;
	}
}

inline void Serial::init_gpio() const
{
	const gpio::pin_def_t rx_def = {gpio::INPUT_FLOATING, gpio::SPEED_INPUT};
	const gpio::pin_def_t tx_def = {gpio::AF_PUSH_PULL,   gpio::SPEED_50MHZ};

	gpio::pin_t rx_pin;
	gpio::pin_t tx_pin;

	switch(device_)
	{
		case SERIAL2:
		{
			tx_pin = {gpio::PORT_A, gpio::PIN_2};
			rx_pin = {gpio::PORT_A, gpio::PIN_3};
		}
		break;

		case SERIAL3:
		{
			tx_pin = {gpio::PORT_A, gpio::PIN_10};
			rx_pin = {gpio::PORT_A, gpio::PIN_11};
		}
		break;

		default:
		case SERIAL1:
		{
			tx_pin = {gpio::PORT_A, gpio::PIN_9};
			rx_pin = {gpio::PORT_A, gpio::PIN_10};
		}
		break;
	}

	gpio::GPIO(rx_pin).init(rx_def);
	gpio::GPIO(tx_pin).init(tx_def);
}

void Serial::enable_interrupt() const
{
	switch(device_)
	{
		case SERIAL1:
		{
			NVIC_EnableIRQ(USART1_IRQn);
		}
		break;

		case SERIAL2:
		{
			NVIC_EnableIRQ(USART2_IRQn);
		}
		break;

		case SERIAL3:
		{
			NVIC_EnableIRQ(USART3_IRQn);
		}
		break;
	}
}

void Serial::callback()
{
	const uint8_t data = static_cast<uint8_t>(USART_ReceiveData((USART_TypeDef*)register_));

	rx_buffer_.insert(data);
}

//It's a blocking function, functional but not optimized
void Serial::write(const uint8_t byte) const
{
	//wait while TX is not empty
	while(!USART_GetFlagStatus((USART_TypeDef*)register_, USART_FLAG_TXE));

	USART_SendData((USART_TypeDef*)register_, byte);
}


/*
 * Low level functions
 */

void __irq_usart1(void)
{
	if(serial_callbacks[0])
	{
		serial_callbacks[0]->callback();
	}
}

void __irq_usart2(void)
{
	if(serial_callbacks[1])
	{
		serial_callbacks[1]->callback();
	}
}

void __irq_usart3(void)
{
	if(serial_callbacks[2])
	{
		serial_callbacks[2]->callback();
	}
}

void putch(unsigned char c)
{

}

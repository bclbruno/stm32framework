/*
 * Print.cpp
 *
 *  Created on: 21 de abr de 2018
 *      Author: Bruno
 */

#include "Print.h"
#include <stdarg.h>
#include <string.h>
#include <stdio.h>

using namespace print;

void Print::print(const char* str) const
{
	while(*str)
	{
		write(*str++);
	}
}

void Print::printf(const char* format, ...)
{
	char buffer[256];
	memset(buffer, 0, sizeof(buffer));
	va_list args;

	va_start(args, format);
	vsnprintf(buffer, sizeof(buffer), format, args);
	va_end(args);

	print(buffer);
}

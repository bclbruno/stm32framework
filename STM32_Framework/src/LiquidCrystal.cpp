/*
 * LiquidCrystal.cpp
 *
 *  Created on: 21 de abr de 2018
 *      Author: Bruno
 */

#include "LiquidCrystal.h"
#include "System.h"

using namespace lcd;

// When the display powers up, it is configured as follows:
//
// 1. Display clear
// 2. Function set:
//    DL = 1; 8-bit interface data
//    N = 0; 1-line display
//    F = 0; 5x8 dot character font
// 3. Display on/off control:
//    D = 0; Display off
//    C = 0; Cursor off
//    B = 0; Blinking off
// 4. Entry mode set:
//    I/D = 1; Increment by 1
//    S = 0; No shift
//
// Note, however, that resetting the Arduino doesn't reset the LCD, so we
// can't assume that its in that state when a sketch starts (and the
// LiquidCrystal constructor is called).

LiquidCrystal::LiquidCrystal(const gpio::pin_t rs, const gpio::pin_t rw, const gpio::pin_t enable,
		const gpio::pin_t d0, const gpio::pin_t d1, const gpio::pin_t d2, const gpio::pin_t d3,
		const gpio::pin_t d4, const gpio::pin_t d5, const gpio::pin_t d6, const gpio::pin_t d7):
	rs_pin_(rs),
	rw_pin_(rw),
	enable_pin_(enable),
	data_pins_({d0, d1, d2, d3, d4, d5, d6, d7}),
	has_rw_pin_(true),
	displayfunction_(MODE_8BIT | LINES_1 | DOTS_5x8),
	displaycontrol_(0),
	displaymode_(0),
	numlines_(1)
{

}

LiquidCrystal::LiquidCrystal(const gpio::pin_t rs, const gpio::pin_t enable,
		const gpio::pin_t d0, const gpio::pin_t d1, const gpio::pin_t d2, const gpio::pin_t d3,
		const gpio::pin_t d4, const gpio::pin_t d5, const gpio::pin_t d6, const gpio::pin_t d7):
	rs_pin_(rs),
	rw_pin_(unused_pin),
	enable_pin_(enable),
	data_pins_({d0, d1, d2, d3, d4, d5, d6, d7}),
	has_rw_pin_(false),
	displayfunction_(MODE_8BIT | LINES_1 | DOTS_5x8),
	displaycontrol_(0),
	displaymode_(0),
	numlines_(1)
{

}

LiquidCrystal::LiquidCrystal(const gpio::pin_t rs, const gpio::pin_t rw, const gpio::pin_t enable,
		const gpio::pin_t d0, const gpio::pin_t d1, const gpio::pin_t d2, const gpio::pin_t d3):
	rs_pin_(rs),
	rw_pin_(rw),
	enable_pin_(enable),
	data_pins_({d0, d1, d2, d3, unused_pin, unused_pin, unused_pin, unused_pin}),
	has_rw_pin_(true),
	displayfunction_(MODE_4BIT | LINES_1 | DOTS_5x8),
	displaycontrol_(0),
	displaymode_(0),
	numlines_(1)
{

}

LiquidCrystal::LiquidCrystal(const gpio::pin_t rs, const gpio::pin_t enable,
		const gpio::pin_t d0, const gpio::pin_t d1, const gpio::pin_t d2, const gpio::pin_t d3):
	rs_pin_(rs),
	rw_pin_(unused_pin),
	enable_pin_(enable),
	data_pins_({d0, d1, d2, d3, unused_pin, unused_pin, unused_pin, unused_pin}),
	has_rw_pin_(false),
	displayfunction_(MODE_4BIT | LINES_1 | DOTS_5x8),
	displaycontrol_(0),
	displaymode_(0),
	numlines_(1)
{

}

void LiquidCrystal::init(const uint8_t cols, const uint8_t lines, const uint8_t dotsize)
{
	if (lines > 1)
	{
		displayfunction_ |= LINES_2;
	}
	numlines_ = lines;

	setRowOffsets(0x00, 0x40, 0x00 + cols, 0x40 + cols);

	// for some 1 line displays you can select a 10 pixel high font
	if ((dotsize != DOTS_5x8) && (lines == 1))
	{
		displayfunction_ |= DOTS_5x10;
	}

	init_gpio();

	// SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
	// according to datasheet, we need at least 40ms after power rises above 2.7V
	// before sending commands. Arduino can turn on way before 4.5V so we'll wait 50
	core::delay_us(50000);
	// Now we pull both RS and R/W low to begin commands
	rs_pin_.set_low();
	enable_pin_.set_low();
	if(has_rw_pin_)
	{
		rw_pin_.set_low();
	}

	//put the LCD into 4 bit or 8 bit mode
	if (!(displayfunction_ & MODE_8BIT))
	{
		// this is according to the hitachi HD44780 datasheet
		// figure 24, pg 46

		// we start in 8bit mode, try to set 4 bit mode
		write4bits(0x03);
		core::delay_us(4500); // wait min 4.1ms

		// second try
		write4bits(0x03);
		core::delay_us(4500); // wait min 4.1ms

		// third go!
		write4bits(0x03);
		core::delay_us(150);

		// finally, set to 4-bit interface
		write4bits(0x02);
	}
	else
	{
		// this is according to the hitachi HD44780 datasheet
		// page 45 figure 23

		// Send function set command sequence
		command(FUNCTION_SET | displayfunction_);
		core::delay_us(4500);  // wait more than 4.1ms

		// second try
		command(FUNCTION_SET | displayfunction_);
		core::delay_us(150);

		// third go
		command(FUNCTION_SET | displayfunction_);
	}

	// finally, set # lines, font size, etc.
	command(FUNCTION_SET | displayfunction_);

	// turn the display on with no cursor or blinking default
	displaycontrol_ = DISPLAY_ON | CURSOR_OFF | BLINK_OFF;
	display();

	// clear it off
	clear();

	// Initialize to default text direction (for romance languages)
	displaymode_ = ENTRY_LEFT | ENTRY_SHIFT_DECREMENT;
	// set the entry mode
	command(ENTRY_MODE_SET | displaymode_);

}

inline void LiquidCrystal::setRowOffsets(const uint8_t row0, const uint8_t row1, const uint8_t row2, const uint8_t row3)
{
	row_offsets_[0] = row0;
	row_offsets_[1] = row1;
	row_offsets_[2] = row2;
	row_offsets_[3] = row3;
}

/********** high level commands, for the user! */
void LiquidCrystal::clear()
{
	command (CLEAR_DISPLAY);  // clear display, set cursor position to zero
	core::delay_us(2000);  // this command takes a long time!
}

void LiquidCrystal::home()
{
	command (RETURN_HOME);  // set cursor position to zero
	core::delay_us(2000);  // this command takes a long time!
}

void LiquidCrystal::setCursor(uint8_t col, uint8_t row)
{
	const size_t max_lines = sizeof(row_offsets_) / sizeof(*row_offsets_);
	if (row >= max_lines) {
		row = max_lines - 1;    // we count rows starting w/0
	}
	if (row >= numlines_) {
		row = numlines_ - 1;    // we count rows starting w/0
	}

	command(SET_DDRAM_ADDR | (col + row_offsets_[row]));
}

// Turn the display on/off (quickly)
void LiquidCrystal::noDisplay()
{
	displaycontrol_ &= ~DISPLAY_ON;
	command(DISPLAY_CONTROL | displaycontrol_);
}
void LiquidCrystal::display()
{
	displaycontrol_ |= DISPLAY_ON;
	command(DISPLAY_CONTROL | displaycontrol_);
}

// Turns the underline cursor on/off
void LiquidCrystal::noCursor()
{
	displaycontrol_ &= ~CURSOR_ON;
	command(DISPLAY_CONTROL | displaycontrol_);
}
void LiquidCrystal::cursor()
{
	displaycontrol_ |= CURSOR_ON;
	command(DISPLAY_CONTROL | displaycontrol_);
}

// Turn on and off the blinking cursor
void LiquidCrystal::noBlink()
{
	displaycontrol_ &= ~BLINK_ON;
	command(DISPLAY_CONTROL | displaycontrol_);
}
void LiquidCrystal::blink()
{
	displaycontrol_ |= BLINK_ON;
	command(DISPLAY_CONTROL | displaycontrol_);
}

// These commands scroll the display without changing the RAM
void LiquidCrystal::scrollDisplayLeft(void)
{
	command(CURSOR_SHIFT | DISPLAY_MOVE | MOVE_LEFT);
}
void LiquidCrystal::scrollDisplayRight(void)
{
	command(CURSOR_SHIFT | DISPLAY_MOVE | MOVE_RIGHT);
}

// This is for text that flows Left to Right
void LiquidCrystal::leftToRight(void)
{
	displaymode_ |= ENTRY_LEFT;
	command(ENTRY_MODE_SET | displaymode_);
}

// This is for text that flows Right to Left
void LiquidCrystal::rightToLeft(void)
{
	displaymode_ &= ~ENTRY_LEFT;
	command(ENTRY_MODE_SET | displaymode_);
}

// This will 'right justify' text from the cursor
void LiquidCrystal::autoscroll(void)
{
	displaymode_ |= ENTRY_SHIFT_INCREMENT;
	command(ENTRY_MODE_SET | displaymode_);
}

// This will 'left justify' text from the cursor
void LiquidCrystal::noAutoscroll(void)
{
	displaymode_ &= ~ENTRY_SHIFT_INCREMENT;
	command(ENTRY_MODE_SET | displaymode_);
}

// Allows us to fill the first 8 CGRAM locations
// with custom characters
void LiquidCrystal::createChar(uint8_t location, uint8_t charmap[])
{
	location &= 0x7; // we only have 8 locations 0-7
	command(SET_CGRAM_ADDR | (location << 3));
	for (int i = 0; i < 8; i++)
	{
		write(charmap[i]);
	}
}

/*********** mid level commands, for sending data/cmds */

inline void LiquidCrystal::command(uint8_t value)
{
	send(value, false);
}

void LiquidCrystal::write(const uint8_t value) const
{
	send(value, true);
}

/************ low level data pushing commands **********/

// write either command or data, with automatic 4/8-bit selection
void LiquidCrystal::send(const uint8_t value, const bool mode) const
{
	rs_pin_.write(mode);

	// if there is a RW pin indicated, set it low to Write
	if (has_rw_pin_)
	{
		rw_pin_.set_low();
	}

	if (displayfunction_ & MODE_8BIT)
	{
		write8bits(value);
	}
	else
	{
		write4bits(value >> 4);
		write4bits(value);
	}
}

void LiquidCrystal::pulseEnable(void) const
{
	enable_pin_.set_low();
	core::delay_us(1);
	enable_pin_.set_high();
	core::delay_us(1);		// enable pulse must be >450ns
	enable_pin_.set_low();
	core::delay_us(100);	// commands need > 37us to settle
}

inline void LiquidCrystal::write4bits(const uint8_t value) const
{
	for (int i = 0; i < 4; ++i)
	{
		data_pins_[i].write((value >> i) & 0x01);
	}

	pulseEnable();
}

inline void LiquidCrystal::write8bits(const uint8_t value) const
{
	for (int i = 0; i < 8; ++i)
	{
		data_pins_[i].write((value >> i) & 0x01);
	}

	pulseEnable();
}

inline void LiquidCrystal::init_gpio()
{
	const gpio::pin_def_t default_config = {gpio::OUTPUT_PUSH_PULL,  gpio::SPEED_2MHZ};

	if(has_rw_pin_)
	{
		rw_pin_.init(default_config);
	}

	rs_pin_.init(default_config);
	enable_pin_.init(default_config);

	for(int i = 0; i < 4; ++i)
	{
		data_pins_[i].init(default_config);
	}

	if (displayfunction_ & MODE_8BIT)
	{
		for(int i = 4; i < 8; ++i)
		{
			data_pins_[i].init(default_config);
		}
	}
}

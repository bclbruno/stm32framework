/*
 * ADC.cpp
 *
 *  Created on: 16 de dez de 2017
 *      Author: Bruno
 */

#include "ADC.h"
#include "stm32f10x.h"
#include "GPIO.h"
#include "System.h"

using namespace adc;

namespace
{
	const uint32_t ADC_TIMEOUT = 100;//ms

#ifdef STM32F10X_MD
	const gpio::pin_t pin_map[] = {
			{gpio::PORT_A, gpio::PIN_0}, //CHANNEL 0
			{gpio::PORT_A, gpio::PIN_1}, //CHANNEL 1
			{gpio::PORT_A, gpio::PIN_2}, //CHANNEL 2
			{gpio::PORT_A, gpio::PIN_3}, //CHANNEL 3
			{gpio::PORT_A, gpio::PIN_4}, //CHANNEL 4
			{gpio::PORT_A, gpio::PIN_5}, //CHANNEL 5
			{gpio::PORT_A, gpio::PIN_6}, //CHANNEL 6
			{gpio::PORT_A, gpio::PIN_7}, //CHANNEL 7
			{gpio::PORT_B, gpio::PIN_0}, //CHANNEL 8
			{gpio::PORT_B, gpio::PIN_1}, //CHANNEL 9
			{gpio::PORT_C, gpio::PIN_0}, //CHANNEL 10
			{gpio::PORT_C, gpio::PIN_1}, //CHANNEL 11
			{gpio::PORT_C, gpio::PIN_2}, //CHANNEL 12
			{gpio::PORT_C, gpio::PIN_3}, //CHANNEL 13
			{gpio::PORT_C, gpio::PIN_4}, //CHANNEL 14
			{gpio::PORT_C, gpio::PIN_5}, //CHANNEL 15
	};
#endif
}

ADC::ADC(const device_t device, const channel_t channel):
		device_(device),
		channel_(channel)
{
	switch(device)
	{
		case ADC_1:	register_ = (HWRegister)ADC1; 	break;
		case ADC_2:	register_ = (HWRegister)ADC2; 	break;
		case ADC_3:	register_ = (HWRegister)ADC3; 	break;
	}
}

void ADC::init()
{
	ADC_InitTypeDef init_struct;

	init_struct.ADC_Mode = ADC_Mode_Independent;
	init_struct.ADC_DataAlign = ADC_DataAlign_Right;
	init_struct.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; //SW_start
	init_struct.ADC_ContinuousConvMode = DISABLE;
	init_struct.ADC_NbrOfChannel = 1;
	init_struct.ADC_ScanConvMode = DISABLE;

	enable_clock();
	init_gpio();

	ADC_Init((ADC_TypeDef*)register_, &init_struct);
	ADC_RegularChannelConfig((ADC_TypeDef*)register_, channel_, 1, ADC_SampleTime_1Cycles5);
	ADC_Cmd((ADC_TypeDef*)register_, ENABLE);

	ADC_StartCalibration((ADC_TypeDef*)register_);
	while(ADC_GetCalibrationStatus((ADC_TypeDef*)register_));
}

void ADC::enable_clock() const
{
#if defined (STM32F10X_LD) || defined (STM32F10X_LD_VL) || defined (STM32F10X_MD) || defined (STM32F10X_MD_VL) || defined (STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined (STM32F10X_XL) || defined (STM32F10X_CL)
	switch(device_)
	{
		case ADC_1: RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);	break;
		case ADC_2: RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2, ENABLE);	break;
		case ADC_3: RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC3, ENABLE);	break;
	}
#endif
}

void ADC::init_gpio() const
{
	gpio::GPIO(pin_map[channel_]).init({gpio::ANALOG, gpio::SPEED_INPUT});
}

uint16_t ADC::single_read() const
{
	const uint32_t initial_time = core::millis();

	ADC_RegularChannelConfig((ADC_TypeDef*)register_, channel_, 1, ADC_SampleTime_1Cycles5);
	ADC_SoftwareStartConvCmd((ADC_TypeDef*)register_, ENABLE);
	ADC_ClearFlag((ADC_TypeDef*)register_, ADC_FLAG_EOC);
	while(ADC_GetFlagStatus((ADC_TypeDef*)register_, ADC_FLAG_EOC) != SET)
	{
		if((core::millis() - initial_time) > ADC_TIMEOUT)
		{
			return ADC_VALUE_ERROR;
		}
	}

	return ADC_GetConversionValue((ADC_TypeDef*)register_);
}

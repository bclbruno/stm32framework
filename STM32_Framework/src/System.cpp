#include "System.h"
#include "stm32f10x.h"

/**
  * @brief  Resets the RCC clock configuration to the factory default.
  * @param  None
  * @retval None
  */
void core::reset_clock()
{
	RCC_DeInit();
}

/**
  * @brief  Enables selected clock.
  * @param  clock: clock source to verify.
  *   This parameter can be one of the following values:
  *     @arg IHS_CLK
  *     @arg EHS_CLK
  *     @arg ILS_CLK
  *     @arg ELS_CLK
  *     @arg PLL_CLK
  * @retval None
  */
void core::enable_clock(const uint32_t clock)
{
	switch(clock)
	{
		case IHS_CLK:	RCC_HSICmd(ENABLE);			break;
		case EHS_CLK:	RCC_HSEConfig(RCC_HSE_ON);	break;
		case ILS_CLK:	RCC_LSICmd(ENABLE);			break;
		case ELS_CLK:	RCC_LSEConfig(RCC_LSE_ON);	break;
		case PLL_CLK:	RCC_PLLCmd(ENABLE);			break;
	}
}

/**
  * @brief  Disables selected clock.
  * @param  clock: clock source to verify.
  *   This parameter can be one of the following values:
  *     @arg IHS_CLK
  *     @arg EHS_CLK
  *     @arg ILS_CLK
  *     @arg ELS_CLK
  *     @arg PLL_CLK
  * @retval None
  */
void core::disable_clock(const uint32_t clock)
{
	switch(clock)
	{
		case IHS_CLK:	RCC_HSICmd(DISABLE);		break;
		case EHS_CLK:	RCC_HSEConfig(RCC_HSE_OFF);	break;
		case ILS_CLK:	RCC_LSICmd(DISABLE);		break;
		case ELS_CLK:	RCC_LSEConfig(RCC_LSE_OFF);	break;
		case PLL_CLK:	RCC_PLLCmd(DISABLE);		break;
	}
}

/**
  * @brief  Checks if clock is ready.
  * @param  clock: clock source to verify.
  *   This parameter can be one of the following values:
  *     @arg IHS_CLK
  *     @arg EHS_CLK
  *     @arg ILS_CLK
  *     @arg ELS_CLK
  *     @arg PLL_CLK
  * @retval Returns true if the clock is ready.
  */
bool core::is_clock_enabled(const uint32_t clock)
{
	const uint32_t max_wait = 0x0500;
	volatile uint32_t counter = 0;
	FlagStatus clk_flag = RESET;

	do
	{
		switch(clock)
		{
			case IHS_CLK:	clk_flag = RCC_GetFlagStatus(RCC_FLAG_HSIRDY);	break;
			case EHS_CLK:	clk_flag = RCC_GetFlagStatus(RCC_FLAG_HSERDY);	break;
			case ILS_CLK:	clk_flag = RCC_GetFlagStatus(RCC_FLAG_LSIRDY);	break;
			case ELS_CLK:	clk_flag = RCC_GetFlagStatus(RCC_FLAG_LSERDY);	break;
			case PLL_CLK:	clk_flag = RCC_GetFlagStatus(RCC_FLAG_PLLRDY);	break;
		}

		++counter;
	}while((clk_flag == RESET) && (counter != max_wait) );

	return clk_flag;
}

/**
  * @brief  Configures the system clock (SYSCLK).
  * @param  clk_source: specifies the clock source used as system clock.
  *   This parameter can be one of the following values:
  *     @arg IHS_CLK: HSI selected as system clock
  *     @arg EHS_CLK: HSE selected as system clock
  *     @arg PLL_CLK: PLL selected as system clock
  * @retval None
  */
void core::set_sysclk_source(const uint32_t clk_source)
{
	switch(clk_source)
	{
		case IHS_CLK:	RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI);		break;
		case EHS_CLK:	RCC_SYSCLKConfig(RCC_SYSCLKSource_HSE);		break;
		case PLL_CLK:	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);	break;
	}
}

/**
  * @brief  Gets current SYSCLK source
  * @param  None
  * @retval SYSCLK source
  *  This value can be:
  * 	@arg
  */
uint32_t core::get_sysclk_source()
{
	const uint32_t clk_source = RCC_GetSYSCLKSource();

	switch(clk_source)
	{
		case 0x04:	return EHS_CLK;
		case 0x08:	return PLL_CLK;
		default:	return IHS_CLK;
	}
}

/**
  * @brief  Configures the PLL clock (PLLCLK).
  * @param  clk_source: specifies the clock source used as system clock.
  *   This parameter can be one of the following values:
  *     @arg IHS_CLK: HSI selected as pll input clock
  *     @arg EHS_CLK: HSE selected as pll input clock
  *     @arg EHS_CLK_DIV_2: PLL selected as pll input clock
  * @param pll_mult: specifies how many times the input frequency will be multiplied.
  *   This parameter ranges from 2 to 16
  * @retval None
  */
void core::set_pll_source(const uint32_t clk_source, const uint32_t pll_mult)
{
	const uint32_t mult_mask = RCC_PLLMul_2 + (pll_mult - 2)*RCC_PLLMul_3;

	switch(clk_source)
	{
		case IHS_CLK:		RCC_PLLConfig(RCC_PLLSource_HSI_Div2, mult_mask); break;
		case EHS_CLK:		RCC_PLLConfig(RCC_PLLSource_HSE_Div1, mult_mask); break;
		case EHS_CLK_DIV_2:	RCC_PLLConfig(RCC_PLLSource_HSE_Div1, mult_mask); break;
	}
}

/**
  * @brief  Configures the Peripheral Bus clock (AHBCLK) speed.
  * 	It is calculates as AHBCLK = SYSCLK/clk_pre_scale
  * @param  clk_pre_scale: specifies the peripheral pre scale
  *   This parameter ranges can be:
  * 	@arg PRE_SCALE_1
  * 	@arg PRE_SCALE_2
  * 	@arg PRE_SCALE_4
  * 	@arg PRE_SCALE_8
  * 	@arg PRE_SCALE_16
  * 	@arg PRE_SCALE_32
  * 	@arg PRE_SCALE_64
  * 	@arg PRE_SCALE_128
  * 	@arg PRE_SCALE_256
  * 	@arg PRE_SCALE_512
  * @retval None
  */
void core::set_peripheral_clk(const uint32_t clk_pre_scale)
{
	uint32_t pre_scale_mask = 0x00000000;
	if(clk_pre_scale > PRE_SCALE_1)
	{
		pre_scale_mask = RCC_SYSCLK_Div2 + ((clk_pre_scale - PRE_SCALE_2) << 4);
	}

	RCC_HCLKConfig(pre_scale_mask);
}

/**
  * @brief  Configures the High Speed Peripheral BUS clock (PCLK2) speed.
  * 	It is calculates as PCLK2 = AHBCLK/clk_pre_scale
  * @param  clk_pre_scale: specifies the peripheral pre scale
  *   This parameter ranges can be:
  *     @arg PRE_SCALE_1
  * 	@arg PRE_SCALE_2
  * 	@arg PRE_SCALE_4
  * 	@arg PRE_SCALE_8
  * 	@arg PRE_SCALE_16
  * @retval None
  */
void core::set_hs_peripheral_clk(const uint32_t clk_pre_scale)
{
	uint32_t pre_scale_mask = 0x00000000;
	if(clk_pre_scale > PRE_SCALE_1)
	{
		pre_scale_mask = RCC_HCLK_Div2 + ((clk_pre_scale - PRE_SCALE_2) << 8);
	}

	RCC_PCLK2Config(pre_scale_mask);
}

/**
  * @brief  Configures the High Speed Peripheral BUS clock (PCLK1) speed.
  * 	It is calculates as PCLK1 = AHBCLK/clk_pre_scale. This value has a limit
  * 	(processor dependent, check datasheet)
  * @param  clk_pre_scale: specifies the peripheral pre scale
  *   This parameter ranges can be:
  *     @arg PRE_SCALE_1
  * 	@arg PRE_SCALE_2
  * 	@arg PRE_SCALE_4
  * 	@arg PRE_SCALE_8
  * 	@arg PRE_SCALE_16
  * @retval None
  */
void core::set_ls_peripheral_clk(const uint32_t clk_pre_scale)
{
	uint32_t pre_scale_mask = 0x00000000;
	if(clk_pre_scale > PRE_SCALE_1)
	{
		pre_scale_mask = RCC_HCLK_Div2 + ((clk_pre_scale - PRE_SCALE_2) << 8);
	}

	RCC_PCLK1Config(pre_scale_mask);
}

/**
  * @brief  Sets Vector Table position in memory
  * @param  None
  * @retval None
  */
void core::set_vector_table()
{
	#ifdef VECT_TAB_SRAM
		NVIC_SetVectorTable(NVIC_VectTab_RAM, VECT_TAB_OFFSET); /* Vector Table Relocation in Internal SRAM. */
	#else
		NVIC_SetVectorTable(NVIC_VectTab_FLASH, VECT_TAB_OFFSET); /* Vector Table Relocation in Internal FLASH. */
	#endif
}

/**
  * @brief  Configures ADC clock. The value is calculated as
  * 	ADCCLK = PCLK2 / clk_prescale. This value has a limit
  * 	(processor dependent, check datasheet)
  * @param  clk_pre_scale: specifies the peripheral pre scale
  *   This parameter ranges can be:
  * 	@arg ADC_PRE_SCALE_2
  * 	@arg ADC_PRE_SCALE_4
  * 	@arg ADC_PRE_SCALE_6
  * 	@arg ADC_PRE_SCALE_8
  * @retval None
  */
void core::set_adc_clock(const uint32_t clk_pre_scale)
{
	uint32_t pre_scale_mask = 0x00000000;
	if(clk_pre_scale > ADC_PRE_SCALE_2)
	{
		pre_scale_mask = RCC_PCLK2_Div4 + ((clk_pre_scale - ADC_PRE_SCALE_4) << 14);
	}

	RCC_ADCCLKConfig(pre_scale_mask);
}
